export interface City {
  cityId: number
  countryId: number
  cityNameKr: string
  cityNameEn: string
  imgUrl?: string
}
