export interface Pageing {
  page: number
  size: number
  sort?: string
}