export default interface PagingProps {
  content: []
  empty: boolean
  first: boolean
  last: boolean
  number: number
  numberOfElements: number
  pageable: string
  size: number
  sort: object
  totalElements: number
  totalPages:number
}