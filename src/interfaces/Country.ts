export interface Country {
  id: number,
  name: string,
  nameEng: string,
  continentName: string,
  img: string,
}