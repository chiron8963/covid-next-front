export type MemberSignupRequest = {
    name: string,
    email: string,
    password: string,
    gender: string,
    phone: string,
}