export interface News {
  id: string
  title: string
  content: string
  img?: string
  createdAt?: Date
}