export interface CovidContentProps {
  code: string
  continentName: string
  continentNameEn: string
  countryName: string
  countryNameEn: string
  covidCountriesData: CovidCountriesData
  id: number
  imgUrl: string
  latestDate: string
  level: number
  levelName: string
  population: number
  todayConfirmed: number
  todayDeaths: number
}

interface CovidCountriesData {
  code: string
  coordinates: Coordinates
  latest_data: LatestData
  name: string
  population: number
  timeline: TimeLine[]
  today: object
  updated_at: string
}

interface Coordinates {
  latitude: number
  longitude: number
}

interface LatestData {
  calculated: Calculated
  confirmed: number
  critical: number
  deaths: number
  recovered: number
}

interface Calculated {
  cases_per_million_population: number
  death_rate: number
  recovery_rate: number
}

interface TimeLine {
  active: number
  confirmed: number
  date: string
  deaths: number
  new_confirmed: number
  new_deaths: number
  new_recovered: number
  recovered: number
  updated_at: string
}