export type MemberLoginRequest = {
  email: string
  password: string
}
