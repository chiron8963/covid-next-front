export interface CityRequest {
  countryId: number
  cityNameKr: string
  cityNameEn: string
}
