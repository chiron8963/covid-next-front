const getContinentMock = {
  Asia: [
    {
      id: 1,
      title: '뉴질랜드',
      name: 'New Zealand',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_25&file_sn=1',
    },
    {
      id: 2,
      title: '뉴질랜드',
      name: 'New Zealand',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_25&file_sn=1',
    },
    {
      id: 3,
      title: '뉴질랜드',
      name: 'New Zealand',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_25&file_sn=1',
    },
    {
      id: 4,
      title: '뉴질랜드',
      name: 'New Zealand',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_25&file_sn=1',
    },
    {
      id: 5,
      title: '뉴질랜드',
      name: 'New Zealand',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_25&file_sn=1',
    },
  ],
  Africa: [
    {
      id: 0,
      title: '나이지리아',
      name: 'Nigeria',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_18&file_sn=1',
    },
    {
      id: 1,
      title: '나이지리아',
      name: 'Nigeria',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_18&file_sn=1',
    },
    {
      id: 2,
      title: '나이지리아',
      name: 'Nigeria',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_18&file_sn=1',
    },
    {
      id: 3,
      title: '나이지리아',
      name: 'Nigeria',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_18&file_sn=1',
    },
  ],
  America: [
    {
      id: 0,
      title: '캐나다',
      name: 'Canada',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_204&file_sn=1',
    },
    {
      id: 1,
      title: '캐나다',
      name: 'Canada',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_204&file_sn=1',
    },
    {
      id: 2,
      title: '캐나다',
      name: 'Canada',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_204&file_sn=1',
    },
    {
      id: 3,
      title: '캐나다',
      name: 'Canada',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_204&file_sn=1',
    },
  ],
  Europe: [
    {
      id: 0,
      title: '체코',
      name: 'Czech Republic',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_195&file_sn=1',
    },
    {
      id: 1,
      title: '체코',
      name: 'Czech Republic',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_195&file_sn=1',
    },
    {
      id: 2,
      title: '체코',
      name: 'Czech Republic',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_195&file_sn=1',
    },
    {
      id: 3,
      title: '체코',
      name: 'Czech Republic',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_195&file_sn=1',
    },
    {
      id: 4,
      title: '체코',
      name: 'Czech Republic',
      imgUrl:
        'http://www.0404.go.kr/imgsrc.mofa?atch_file_id=COUNTRY_195&file_sn=1',
    },
  ],
}

export default getContinentMock
