import axios from 'axios'

const client = axios.create({})

client.defaults.baseURL =
  process.env.NODE_ENV === 'development'
    ? process.env.NEXT_PUBLIC_API_URL
    : process.env.NEXT_PUBLIC_API_URL

export default client
