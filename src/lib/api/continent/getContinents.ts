import client from '../../client'

export const getContinents = async () => {
  const response = await client.get(`/api/v2/continents`)
  return response.data
}
