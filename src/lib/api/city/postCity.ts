import client from '../../client'
import { CityRequest } from '../../../interfaces/CityRequest'

export const postCity = async (city: CityRequest) => {
  await client.post(`/api/v2/city`, city)
}
