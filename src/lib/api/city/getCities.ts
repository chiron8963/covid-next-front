import client from '../../client'

export const getCities = async (id) => {
  const response = await client.get(`/api/v2/cities/${id}`)
  return response.data
}
