import useSWR from 'swr'
import { getMember } from '../member/getMember'
import { useRecoilState } from 'recoil'
import { memberState } from '../../../recoil/member'
import { useEffect } from 'react'

export default function useUser() {
  const { data, mutate, error } = useSWR('/member/me', getMember)
  const loggedIn: boolean = !error && data && data.socialCertification
  const [member, setMember] = useRecoilState(memberState)

  useEffect(() => {
    setMember(data)
  })
  return {
    loggedIn,
    user: data,
    mutate,
  }
}
