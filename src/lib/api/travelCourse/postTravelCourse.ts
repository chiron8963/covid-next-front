import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN } from '../../../constants'

export const postTravelCourse = async (requestTravelCourse) => {
  const accessTokenByCookie = cookie.load(ACCESS_TOKEN)
  client.defaults.headers.common['Authorization'] = accessTokenByCookie
  const response = await client.post(
    `/api/v2/travel-course`,
    requestTravelCourse,
    {
      headers: {
        Authorization: 'Bearer ' + accessTokenByCookie,
        'Access-Control-Allow-Origin': '*',
      },
      timeout: 3000,
    },
  )
  return response.data
}
