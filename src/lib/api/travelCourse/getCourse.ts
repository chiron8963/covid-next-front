import client from '../../client'

export const getCourse = async (page: number, size: number, sort: string) => {
  try {
    const coursePage = page != null ? page : 0
    const courseSize = size != null ? size : 10
    const response = await client.get('/api/v2/travel-course', {
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      params: {
        page: page,
        size: size,
        sort: sort,
      },
      timeout: 3000,
    })

    return response.data
  } catch (error) {
    console.log(error)
  }
}
