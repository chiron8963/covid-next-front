import client from '../../client'

export const getTravelCourseSearch = async (
  page: number,
  size: number,
  keyword: string,
) => {
  try {
    const coursePage = page != null ? page : 0
    const courseSize = size != null ? size : 10
    const response = await client.get('/api/v2/travel-course/search', {
      params: {
        keyword: keyword,
        page: coursePage,
        size: courseSize,
      },
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      timeout: 3000,
    })

    return response.data
  } catch (error) {
    console.log(error)
  }
}
