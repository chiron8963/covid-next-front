import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN } from '../../../constants'

export const deleteTravelCourse = async (requestTravelCourse) => {
  client.defaults.headers.common['Authorization'] = cookie.load(ACCESS_TOKEN)
  const response = await client.delete(
    '/api/v2/travel-course/' + requestTravelCourse.id,
  )
  return response.data
}
