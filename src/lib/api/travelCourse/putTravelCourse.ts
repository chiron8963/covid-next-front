import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN } from '../../../constants'

export const putTravelCourse = async (requestTravelCourse) => {
  client.defaults.headers.common['Authorization'] = cookie.load(ACCESS_TOKEN)
  const response = await client.put(
    '/api/v2/travel-course/' + requestTravelCourse.travelCourseId,
    requestTravelCourse,
    {
      timeout: 3000,
    },
  )
  return response.data
}
