import client from '../../client'

export const getCourseDetail = async (id: string | string[]) => {
  try {
    const response = await client.get('/api/v2/travel-course/' + id, {
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      timeout: 3000,
    })
    return response.data
  } catch (error) {
    console.log(error)
  }
}
