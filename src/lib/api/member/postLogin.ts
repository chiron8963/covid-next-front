import client from '../../client'
import { MemberLoginRequest } from '../../../interfaces/MemberLoginRequest'
import setToken from '../../../components/utils/TokenManager'

export const postLogin = async (loginRequest: MemberLoginRequest) => {
  try {
    const response = await client.post(`/auth/login`, loginRequest)
    setToken(response.data.accessToken, response.data.refreshToken)
    return response.data
  } catch (error) {
    console.log(error)
    throw error
  }
}
