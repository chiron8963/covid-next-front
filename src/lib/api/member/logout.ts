import { MemberLoginRequest } from '../../../interfaces/MemberLoginRequest'
import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../constants'

export const logout = async () => {
  try {
    const accessTokenByCookie = cookie.load(ACCESS_TOKEN)
    const refreshTokenByCookie = cookie.load(REFRESH_TOKEN)
    const response = await client.post(`/auth/logout`, {
      accessToken: accessTokenByCookie,
      refreshToken: refreshTokenByCookie,
    })
    return response.data
  } catch (error) {
    console.log(error)
    throw error
  }
}
