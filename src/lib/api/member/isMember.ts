import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN } from '../../../constants'

export const isMember = async (accessTokenByCookie) => {
  if (accessTokenByCookie) {
    try {
      client.defaults.headers.common['Authorization'] = accessTokenByCookie
      const response = await client.get('/member/me', {
        headers: {
          Authorization: 'Bearer ' + accessTokenByCookie,
          'Access-Control-Allow-Origin': '*',
        },
        timeout: 3000,
      })
      return response.data
    } catch (error) {
      console.log(error)
    }
  }
}
