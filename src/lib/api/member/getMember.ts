import client from '../../client'
import cookie from 'react-cookies'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../constants'
import { reissueMember } from './reissueMember'
import setToken from '../../../components/utils/TokenManager'

export const getMember = async (url) => {
  const accessTokenByCookie = cookie.load(ACCESS_TOKEN)
  if (accessTokenByCookie) {
    try {
      client.defaults.headers.common['Authorization'] = accessTokenByCookie
      const response = await client.get(url, {
        headers: {
          Authorization: 'Bearer ' + accessTokenByCookie,
          'Access-Control-Allow-Origin': '*',
        },
        timeout: 3000,
      })
      return response.data
    } catch (error) {
      console.log(error)
      try {
        if (error.contain('500') > 0) {
          const token = await reissueMember()
          cookie.remove(ACCESS_TOKEN)
          cookie.remove(REFRESH_TOKEN)
          await setToken(token.accessToken, token.refreshToken)
          const accessToken = cookie.load(ACCESS_TOKEN)
          client.defaults.headers.common['Authorization'] = accessToken
          const response = await client.get(url, {
            headers: {
              Authorization: 'Bearer ' + accessToken,
              'Access-Control-Allow-Origin': '*',
            },
            timeout: 3000,
          })
          return response.data
        }
      } catch (error2) {
        cookie.remove(ACCESS_TOKEN)
        cookie.remove(REFRESH_TOKEN)
      }
    }
  }
}
