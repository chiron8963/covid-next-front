import client from '../../client'
import { MemberSignupRequest } from '../../../interfaces/MemberSignupRequest'
import setToken from '../../../components/utils/TokenManager'

export const postSignup = async (signupRequest: MemberSignupRequest) => {
  try {
    const response = await client.post(`/auth/signup`, signupRequest)
    return response.data
  } catch (error) {
    throw error
  }
}
