import cookie from 'react-cookies'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../constants'
import client from '../../client'

export const reissueMember = async () => {
  const accessTokenByCookie = cookie.load(ACCESS_TOKEN)
  const refreshTokenByCookie = cookie.load(REFRESH_TOKEN)
  if (accessTokenByCookie) {
    try {
      const response = await client.post('/auth/reissue', {
        accessToken: accessTokenByCookie,
        refreshToken: refreshTokenByCookie,
      })
      return response.data
    } catch (error) {
      console.log(error)
    }
  }
}
