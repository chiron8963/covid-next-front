import client from '../../client'
import { MemberSignupRequest } from '../../../interfaces/MemberSignupRequest'
import setToken from '../../../components/utils/TokenManager'

export const postSignupChange = async (
  signupRequest: MemberSignupRequest,
  accessTokenByCookie: string,
) => {
  try {
    const response = await client.post(`/member/change`, signupRequest, {
      headers: {
        Authorization: 'Bearer ' + accessTokenByCookie,
        'Access-Control-Allow-Origin': '*',
      },
      timeout: 3000,
    })
    return response.data
  } catch (error) {
    throw error
  }
}
