import client from '../../client'

export const getNews = async (page: number, size: number, keyword: string) => {
  const response = await client.get(`/api/v2/covid-information`, {
    params: {
      page: page,
      size: size,
      keyword: keyword,
    },
  })
  return response.data
}
