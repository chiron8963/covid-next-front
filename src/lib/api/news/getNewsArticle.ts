import client from '../../client'

export const getNewsArticle = async (id: string) => {
  const response = await client.get(`/api/v2/covid-information/${id}`)
  return response.data
}
