import client from '../../client'

export const getCovidCountriesData = async (name, page) => {
  const defaultSize = 5
  const response = await client.get(`/api/v2/covid-level/countries`, {
    params: {
      continentName: name,
      page: page,
      size: defaultSize,
    },
  })
  return response.data
}

export const getCovidContinentData = async () => {
  const response = await client.get(`/api/v2/covid-level/continents`)
  return response.data
}
