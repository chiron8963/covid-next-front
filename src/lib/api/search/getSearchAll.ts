import client from '../../client'

export const getSearchAll = async (keyword: string) => {
  try {
    const res = await client.get(`/api/v2/search/all`, {
      params: {
        keyword: keyword,
      },
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      timeout: 3000,
    })
    return res.data
  } catch (e) {
    console.error(e)
  }
}
