import client from '../../client'

export const getCountry = async () => {
  try {
    const response = await client.get(`/api/v2/countries?page=0&size=300`)
    return response.data
  } catch (error) {
    console.log(error)
  }
}
