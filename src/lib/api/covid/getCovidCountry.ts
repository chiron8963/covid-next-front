import client from '../../client'

export const getCovidCountryName = async (name) => {
  const response = await client.get(`api/v2/covid-level/countries/${name}`)
  return response.data
}
