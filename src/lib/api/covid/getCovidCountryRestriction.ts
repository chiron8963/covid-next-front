import client from '../../client'

export const getCovidCountryRestriction = async (code) => {
  const response = await client.get(
    `api/v2/covid-level/country-restriction/${code}`,
  )
  return response.data
}
