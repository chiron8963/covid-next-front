export const SERVER = process.env.NODE_ENV === 'development' ? true : false
export const ACCESS_TOKEN = 'accessToken'
export const REFRESH_TOKEN = 'refreshToken'
export const SERVER_URL = SERVER
  ? 'http://localhost:8080'
  : 'https://server.covid-next.kro.kr:8011'
export const OAUTH2_REDIRECT_URI = SERVER
  ? 'http://localhost:3000/api/oauth2/redirect'
  : 'https://next-covid-kohl.vercel.app/api/oauth2/redirect'

export const GOOGLE_AUTH_URL =
  SERVER_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI
export const KAKAO_AUTH_URL =
  SERVER_URL + '/oauth2/authorize/kakao?redirect_uri=' + OAUTH2_REDIRECT_URI
