export const santoriniColor = {
  santorini_blue: '#3b72e8',
  santorini_gray: '#F0EDF7',
  santorini_indigo: '#112F70',
}