import { styled } from '@material-ui/core'
import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  
  body {
    line-height: 1.5;
    margin: 0;
    font-family: 'NotoSansCJKkr, SanfranciscoPro' !important;
  }

  h2, p {
    margin: 0;
  }

  ul, li {
    list-style: none;
  }

  .global-wrapper {
    //padding-left: 340px;
    //padding-right: 340px;
    margin-left: auto;
    margin-right: auto;
    max-width: 1170px;

  }

  @font-face {
    font-family: 'SanfranciscoPro';
    //src: url("/fonts/sanfrancisco/SF-Pro-Display-Black.otf");
    //src: url("/fonts/sanfrancisco/SF-Pro-Display-Bold.otf");
    //src: url("/fonts/sanfrancisco/SF-Pro-Display-Light.otf");
    //src: url("/fonts/sanfrancisco/SF-Pro-Display-Medium.otf");
    src: url("/fonts/sanfrancisco/SF-Pro-Display-Semibold.otf");
    font-style: normal;
  }

  @font-face {
    font-family: 'NotoSansCJKkr';
    //src: url("/fonts/notoSans/NotoSansCJKkr-Bold.otf");
    //src: url("/fonts/notoSans/NotoSansCJKkr-Light.otf");
    //src: url("/fonts/notoSans/NotoSansCJKkr-Medium.otf");
    //src: url("/fonts/notoSans/NotoSansCJKkr-Thin.otf");
    //src: url("/fonts/notoSans/NotoSansMonoCJKkr-Bold.otf");
    //src: url("/fonts/notoSans/NotoSansMonoCJKkr-Regular.otf");
    src: url("/fonts/notoSans/NotoSansCJKkr-Black.otf");
  }
`

export default GlobalStyle