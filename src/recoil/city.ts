import { atom } from 'recoil'

export const cityState = atom({
  key: 'cityState',
  default: {
    cityId: null,
    cityNameKr : null,
  },
})