import { atom } from 'recoil'

export const memberState = atom({
  key: 'memberState',
  default: {
    id: null,
    email: null,
    name: null,
    subscribeStatus: false,
    displayName: null,
  },
})

export const authenticated = atom({
  key: 'authenticated',
  default: false,
})
