import { Button, Card, CardActions } from '@material-ui/core'
import CardMedia from '@material-ui/core/CardMedia/CardMedia'
import CardContent from '@material-ui/core/CardContent/CardContent'
import Link from 'next/link'
import styled from 'styled-components'
import { StylesProvider } from '@material-ui/core/styles'
import LevelChip from '../tags/LevelChip'
import CardLayout from './CardLayout'

const CardBlock = styled(Card)`
  height: 26em;

  h3,
  h4,
  h5 {
    margin: 0;
  }

  .MuiCardContent-root {
    width: 100%;
    padding: 0;

    .MuiCardMedia-root {
      &.media {
        padding: 1em;
        height: 260px;
        display: flex;
        flex-direction: column;
      }

      .contentsLevel {
        padding-left: 1em;
        padding-right: 1em;
        text-align: center;
      }

      .levelTitle {
        color: dimgray;
      }

      .contents {
        color: white;
        width: 90%;
        background: rgb(2, 0, 36);
        background: linear-gradient(
          90deg,
          rgba(29, 24, 120, 1) 0%,
          rgba(0, 78, 146, 1) 100%
        );
        padding: 0.8em;
        text-align: center;
        margin-top: 6px;

        .top_distance {
          padding-top: 0.3em;
        }

        h3 {
          padding-bottom: 0.5em;
        }

        .today-data {
          //flex-direction: column;
          flex-direction: row;
          justify-content: space-between;
          font-size: 18px;
          font-weight: bold;
        }

        .subTitle {
          font-size: 12px;
        }

        .contentsLeft {
          float: left;
          width: 33%;
          text-align: center;
          border: none;
          border-right: 1px solid white;
        }

        .contentsCenter {
          float: left;
          width: 30%;
          text-align: center;
          border: none;
          border-right: 1px solid white;
        }

        .contentsRight {
          float: left;
          width: 36%;
          border: none;
          text-align: center;
        }

        .rightTitle {
          font-size: 15px;
          height: 25px;
        }
      }
    }
  }

  .country_wrapper {
    position: relative;
    padding: 1em 1em 0.5em 1em;

    .country_img {
      text-align: center;
    }

    img {
      width: 60px;
      height: 40px;
      border-radius: 5px;
      overflow: hidden;
      border: solid 2px #efefef;
      z-index: 30;
    }

    .names {
      text-align: center;
      margin-top: 10px;

      .country_title_kr {
        font-size: 1.5em;
        font-weight: 700;
      }

      .country_title_en {
        font-weight: 500;
        color: dimgray;
      }
    }
  }

  .MuiCardActions-root {
    display: block;
    text-align: center;
  }

  .MuiButton-outlined {
    background-color: #112f70;
    color: white;
  }

  .MuiButton-outlinedPrimary:hover {
    border: 1px solid #3f51b5;
    background-color: #0a1b3f;
  }
`
const HeaderHr = styled.hr`
  color: #efefef;
  border-color: #efefef;
  background-color: #efefef;
  opacity: 0.3;
`
const CountryCover = styled.div`
  width: 96%;
  height: 50px;
  position: absolute;
  left: 6px;
  top: 28px;
  /*background: rgb(0,53,146);
  background: linear-gradient(90deg, rgba(0,53,146,1) 0%, rgba(0,78,146,1) 50%);*/
  z-index: 0;
`

const setTitle = (type) => {
  if (type === null) {
    return '미확인'
  }

  return RestrictionType[type]
}

export const RestrictionType = {
  NO_ENTRY: '입국 금지',
  RECOMMENDATION: '검역 강화',
  QUARANTINED: '입국 가능',
}

const CountryCardItem = ({ item }) => {
  return (
    <StylesProvider>
      <CardLayout>
        <CardBlock>
          <CardContent>
            {item.imgUrl && (
              <CardMedia className="media" title={item.countryNameEn}>
                <div className="country_wrapper">
                  <div className="country_img">
                    <img src={item.imgUrl} alt={item.countryNameEn} />
                  </div>
                  <div className="names">
                    <div className="country_title_kr">{item.countryName}</div>
                    <div className="country_title_en">
                      ({item.countryNameEn})
                    </div>
                    <HeaderHr />
                  </div>
                </div>
                <div className="contentsLevel">
                  <div>
                    <div className="levelTitle">위험도</div>
                    <LevelChip level={item.level} />
                  </div>
                </div>
                <div className="contents">
                  <div className="contentsLeft">
                    <div className="today-data">
                      <div>+{item.todayConfirmed}</div>
                    </div>
                    <div className="subTitle">확진자</div>
                  </div>
                  <div className="contentsCenter">
                    <div className="today-data">
                      <div>+{item.todayDeaths}</div>
                    </div>
                    <div className="subTitle">사망자</div>
                  </div>
                  <div className="contentsRight">
                    <div className="today-data">
                      <div className="rightTitle">
                        {setTitle(item.restrictionType)}
                      </div>
                    </div>
                    <div className="subTitle">여행가능 여부</div>
                  </div>
                </div>
              </CardMedia>
            )}
            <HeaderHr />
          </CardContent>
          <CardActions>
            <Link
              href={{
                pathname: '/country/[name]',
              }}
              as={`/country/${item.countryNameEn}`}>
              <Button
                variant="outlined"
                color="primary"
                href="#outlined-buttons">
                자세히 보기
              </Button>
            </Link>
          </CardActions>
        </CardBlock>
      </CardLayout>
    </StylesProvider>
  )
}

export default CountryCardItem
