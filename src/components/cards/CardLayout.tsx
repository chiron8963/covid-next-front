import { ReactNode } from 'react'
import styled from 'styled-components'

type Props = {
  children?: ReactNode
}

const CardLayoutWrapper = styled.div`
  width: 20em;
  height: 26em;
`

const CardLayout = ({ children }: Props) => {
  return <CardLayoutWrapper>{children}</CardLayoutWrapper>
}

export default CardLayout
