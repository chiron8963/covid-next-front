import React from 'react'
import { Card } from '@material-ui/core'
import CardMedia from '@material-ui/core/CardMedia/CardMedia'
import CardContent from '@material-ui/core/CardContent/CardContent'
import Typography from '@material-ui/core/Typography/Typography'
import CardActionArea from '@material-ui/core/CardActionArea'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Link from 'next/link'

const useStyles = makeStyles({
  root: {
    width: 260,
    height: 300,
  },
  media: {
    height: 220,
  },
})

const CardItem = ({ data }) => {
  const classes = useStyles()
  return (
    <Link
      href={{
        pathname: '/covid-country/[name]',
      }}
      as={`/covid-country/${data.name}`}>
      <Card className={classes.root}>
        <CardActionArea>
          {data.imgUrl && (
            <CardMedia
              className={classes.media}
              image={data.imgUrl}
              title={data.title}
            />
          )}
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {data.name}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {data.name}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Link>
  )
}

export default CardItem
