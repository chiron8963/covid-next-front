import styled from 'styled-components'
import { Card, CardActionArea, CardContent } from '@material-ui/core'
import { StylesProvider } from '@material-ui/core/styles'
import Link from 'next/link'
import React from 'react'

const ThumbnailCardWrapper = styled.div`
  width: 260.5px;
  height: 350px;
  //margin-left: 16px;
  //margin-bottom: 16px;
  margin: 0.5em;
  position: relative;
`

const ThumbnailCardBlock = styled(Card)`
  width: 100%;
  height: 350px;
  padding: 5px;
  text-align: center;
`
const CardImageDiv = styled.div`
  //width: 278.5px;
  //height: 234px;
  width: 100%;
  height: 100%;
`
const CardImageSize = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 240px;
  overflow: hidden;
`
const CardImage = styled.img`
  height: 100%;
  width: 100%;
`
const CardIcon = styled.div`
  position: absolute;
  width: 40px;
  height: 40px;
  background: center / cover url('/images/free-icon-passport-5507802.png');
  border-radius: 50%;
  top: 215px;
  left: 100px;
  border: 2px solid black;
`

const CardContentBlock = styled(CardContent)`
  padding: 0.5rem !important;
`

const CardTitle = styled.div`
  width: 100%;
  padding: 0 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 23px;
  font-weight: bold;
`

const CardSubTitle = styled.div`
  width: 100%;
  padding: 0 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 12px;
  color: dimgray;
  margin-top: 12px;
`

const CardDisplayName = styled.div`
  width: 100%;
  padding: 0 5px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  font-size: 14px;
`
export const thumbNailType = {
  CITY: 'city',
  TRAVEL: 'travel',
}

interface CardType {
  city: string
  travel: string
}

const CardMain = ({ item, type }) => {
  const name = item?.cityName ? item.cityName : item.cityNameKr

  return (
    <StylesProvider>
      <CardActionArea>
        <CardImageDiv>
          <CardImageSize>
            <CardImage src={item.image || '/images/no_image.png'} />
          </CardImageSize>
        </CardImageDiv>
        {/*<CardIcon />*/}
        <CardContentBlock>
          {type ? (
            <>
              <CardSubTitle>
                {item.countryName} / {name}
              </CardSubTitle>
              <CardTitle>{item.courseName}</CardTitle>
              <CardDisplayName>{item.displayName}</CardDisplayName>
            </>
          ) : (
            <>
              <CardSubTitle />
              <CardTitle>{name}</CardTitle>
            </>
          )}
        </CardContentBlock>
      </CardActionArea>
    </StylesProvider>
  )
}

const ThumbnailCard = ({ item, link, type }) => {
  const cardType = type === thumbNailType.TRAVEL
  return (
    <ThumbnailCardWrapper>
      {cardType ? (
        <Link href={`${link}/[id]`} as={`${link}/${item.id}`}>
          <ThumbnailCardBlock>
            <CardMain item={item} type={cardType} />
          </ThumbnailCardBlock>
        </Link>
      ) : (
        <Link
          href={{
            pathname: `/search`,
            query: { keyword: JSON.stringify(item.cityNameKr) },
          }}
          as={`/search`}>
          <ThumbnailCardBlock>
            <CardMain item={item} type={cardType} />
          </ThumbnailCardBlock>
        </Link>
      )}
    </ThumbnailCardWrapper>
  )
}

export default ThumbnailCard
