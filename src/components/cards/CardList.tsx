import CardItem from './CardItem'
import { ReactElement } from 'react'
import * as React from 'react'
import styled from 'styled-components'

const CardListBlock = styled.ul`
  display: flex;
  justify-content: space-between;
  padding: 0;
  margin-top: 10em;
`

const CardList = ({ items, className }) => {
  return (
    <CardListBlock className={className}>
      {items.map((item) => (
        <li key={item.id}>
          <CardItem data={item} />
        </li>
      ))}
    </CardListBlock>
  )
}

export default CardList
