import axios from 'axios'
import cookie from 'react-cookies'
import { ACCESS_TOKEN, REFRESH_TOKEN, SERVER } from '../../constants'

const setToken = (accessToken: string, refreshToken: string) => {
  axios.defaults.headers.Authorization = `Bearer ${accessToken}`
  const expires = new Date()
  expires.setDate(Date.now() + 1000 * 60 * 30)

  cookie.save(ACCESS_TOKEN, accessToken, {
    path: '/',
    expires,
    httpOnly: false, // dev/prod 에 따라 true / false 로 받게 했다.
  })

  cookie.save(REFRESH_TOKEN, refreshToken, {
    path: '/',
    expires,
    httpOnly: false,
  })
}

export default setToken
