import makeStyles from '@material-ui/core/styles/makeStyles'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia/CardMedia'
import CardContent from '@material-ui/core/CardContent/CardContent'
import Typography from '@material-ui/core/Typography/Typography'
import { Card } from '@material-ui/core'
import React from 'react'
import { Skeleton } from '@material-ui/lab'

const useStyles = makeStyles({
  root: {
    width: 260,
    height: 300,
  },
  media: {
    height: 220,
  },
  national_flag: {
    width: 50,
    height: 50,
  },
})

const CountryCardItem = ({ data }) => {
  const classes = useStyles()
  const {
    countryNameKr,
    countryNameEn,
    imgUrl,
    createdAt,
    level,
    id,
    covidCountriesData,
  } = data
  return (
    <Card className={classes.root}>
      <CardActionArea>
        {imgUrl && (
          <CardMedia className={classes.media} title={countryNameEn}>
            <Skeleton
              animation="wave"
              variant="rect"
              width={'100%'}
              height={'100%'}
            />
          </CardMedia>
        )}
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {countryNameKr}
            <span>
              <img className={classes.national_flag} src={imgUrl} />
            </span>
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {countryNameKr}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default CountryCardItem
