import styled from 'styled-components'
import Thumbnail from '../images/Thumbnail'
import { Paper } from '@material-ui/core'
import { StylesProvider } from '@material-ui/core/styles'
import Link from 'next/link'

const CountryItemWrapper = styled(Paper)`
  display: flex;
  height: 100%;
  width: 100%;
  padding: 1.25em;
  margin-bottom: 1.5em;
  //background-color: #f0edf7;

  &&:hover {
    background-color: #f0edf7;
    cursor: pointer;
  }
`
const DescriptionBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1em;
`

const CountryItem = ({ data }) => {
  const {
    countryNameKr,
    countryNameEn,
    imgUrl,
    createdAt,
    level,
    id,
    covidCountriesData,
  } = data

  return (
    // <Link>
    <StylesProvider injectFirst>
      <Link
        href={{
          pathname: '/country/[name]',
        }}
        as={`/country/${data.countryNameEn}`}>
        {data && (
          <CountryItemWrapper>
            <Thumbnail img={imgUrl} name={countryNameEn} />
            <DescriptionBlock>
              <h2>
                {countryNameKr} ({countryNameEn})
              </h2>
              <p>{createdAt}</p>
              <div>코로나 위험 레벨 : {level}</div>
              <div>
                총 확진자 수 :{covidCountriesData.latest_data.confirmed}
              </div>
              <div>
                완치 :{covidCountriesData.latest_data.recovered}(
                {covidCountriesData.latest_data.calculated.recovery_rate.toFixed(
                  2,
                )}
                )
              </div>
              {/*<div>7일 평균 확진자 :</div>*/}
              <div>
                총 사망자 : {covidCountriesData.latest_data.deaths}(
                {covidCountriesData.latest_data.calculated.death_rate.toFixed(
                  2,
                )}
                )
              </div>
              {/*<div>입국 가능 여부 :</div>*/}
            </DescriptionBlock>
          </CountryItemWrapper>
        )}
      </Link>
    </StylesProvider>
  )
}

export default CountryItem
