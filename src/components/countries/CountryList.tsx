import styled from 'styled-components'
import CountryItem from './CountryItem'
import * as React from 'react'
import CountryCardItem from './CountryCardItem'

const CountryWrapper = styled.div`
  padding: 4.5em 0 0;
  margin: 0;
  width: 60%;
  display: flex;
  flex-direction: column;

  .empty {
    display: flex;
  }

  li {
    padding-bottom: 1em;
  }
`

const CountryList = ({ countries, loading }) => {
  console.log('CountryList', countries)
  return (
    <CountryWrapper>
      {!loading && countries.length <= 0 && (
        <div className="empty">
          <h1>해당 지역은 정보가 없습니다.</h1>
        </div>
      )}
      {countries &&
        countries.map((content, idx) => (
          <CountryItem key={content.id} data={content} />
          // <CountryCardItem key={content.id} data={content} />
        ))}
    </CountryWrapper>
  )
}

export default CountryList
