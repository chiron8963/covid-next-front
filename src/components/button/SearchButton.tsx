import styled from 'styled-components'
import SearchIcon from '@material-ui/icons/Search'
import { IconButton } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { useState } from 'react'
import { useRef } from 'react'
import Link from 'next/link'

const SearchWrapper = styled.div`
  //flex-basis: 30em;
  flex: 0 auto;
  padding-right: 0.25em;
  transition: all ease 2s 0s;
`

const SearchButton = styled.button``

const Search = () => {
  // const [isClicked, setIsClicked] = useState<boolean>(false)
  const keywordInput = useRef<HTMLInputElement>(null)

  const findByKeyword = (value): void => {
    // const keyword = value.current.querySelector('#')
    // console.log('findByKeyword ->', keyword)
  }

  const searchClickHandle = (e): void => {
    // isClicked ? setIsClicked(false) : setIsClicked(true)
    console.log(e)
  }
  return (
    <SearchWrapper>
      <Link href="/search">
        <IconButton aria-label="search" color="inherit">
          <SearchIcon />
        </IconButton>
      </Link>
    </SearchWrapper>
  )
}

export default Search
