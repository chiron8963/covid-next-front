import styled from 'styled-components'
import * as React from 'react'
import ContentSkeleton from '../skeleton/ContentSkeletonItem '
import CityItem from './CityItem'
import { ReactElement } from 'react'
import { City } from '../../interfaces/City'
import ThumbnailCard, { thumbNailType } from '../cards/ThumbnailCard'
import { useState } from 'react'
import { Avatar, Chip } from '@material-ui/core'

interface CityListProps {
  cities: City[]
  countryName: string
}

const CityWrapper = styled.div`
  padding: 1em 0 0;
  margin: 0;
  flex: 1;
  display: flex;
  flex-wrap: wrap;

  li {
    padding-bottom: 1em;
  }
`

const CityChip = styled(Chip)`
  margin: 0.3em;
`

const EmojiIcon = () => {
  return <>🏯</>
}

const CityList = ({ cities, countryName }: CityListProps): ReactElement => {
  return (
    <CityWrapper>
      {cities &&
        cities.map((item) => (
          // <ThumbnailCard
          //   key={item.cityId}
          //   item={item}
          //   link={null}
          //   type={thumbNailType.CITY}
          // />
          <CityChip
            key={item.cityId}
            avatar={<Avatar alt="Natacha" src={`/images/icons/city1.png`} />}
            label={item.cityNameKr}
            variant="outlined"
          />
        ))}
      {cities.length <= 0 && (
        <div className="empty">
          <h3>해당 국가는 도시 정보가 없습니다.</h3>
        </div>
      )}
    </CityWrapper>
  )
}

export default CityList
