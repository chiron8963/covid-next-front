import styled from 'styled-components'
import Thumbnail from '../images/Thumbnail'
import { Paper } from '@material-ui/core'
import { StylesProvider } from '@material-ui/core/styles'
import Link from 'next/link'
import Skeleton from '@material-ui/lab/Skeleton'
import { City } from '../../interfaces/City'

interface CountyItemProps {
  item: City
  countryName: string
}

const CountryItemWrapper = styled(Paper)`
  display: flex;
  height: 12em;
  width: 100%;
  padding: 1.25em;
  margin-bottom: 1em;
  //background-color: #f0edf7;

  &&:hover {
    background-color: #f0edf7;
    cursor: pointer;
  }
`
const DescriptionBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1em;
`

const CountryItem = ({ item, countryName }: CountyItemProps) => {
  const { cityId, cityNameKr, cityNameEn, countryId, imgUrl } = item
  return (
    // <Link>
    <StylesProvider injectFirst>
      <Link
        href={{
          pathname: '/city/[name]',
        }}
        as={`/city/${cityNameEn}`}>
        {item && (
          <CountryItemWrapper>
            {imgUrl ? (
              <Thumbnail img={imgUrl} name={cityNameEn} />
            ) : (
              <Skeleton
                animation="wave"
                variant="rect"
                width={170}
                height={132}
              />
            )}

            <DescriptionBlock>
              <h2>
                {cityNameKr} ({cityNameEn})
              </h2>
              <div>⭐⭐⭐⭐⭐</div>
              <div>여행 코스 :</div>
              <div>최근 업데이트 날짜:</div>
            </DescriptionBlock>
          </CountryItemWrapper>
        )}
      </Link>
    </StylesProvider>
  )
}

export default CountryItem
