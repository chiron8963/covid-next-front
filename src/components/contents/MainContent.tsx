import CountryCarousel from '../carousel/CountryCarousel'
import styled from 'styled-components'
import Banner from '../banner/Banner'

const MainContentWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const MainContent = ({ items }) => {
  return (
    <MainContentWrapper>
      <Banner />
      <CountryCarousel
        items={items.asia}
        headerTitle={'아시아/태평양'}
        className="main_item"
      />
      <CountryCarousel
        items={items.america}
        headerTitle={'미주'}
        className="main_item"
      />
      <CountryCarousel
        items={items.europe}
        headerTitle={'유럽'}
        className="main_item"
      />
      <CountryCarousel
        items={items.africa}
        headerTitle={'아프리카/중동'}
        className="main_item"
      />
    </MainContentWrapper>
  )
}

export default MainContent
