import styled from 'styled-components'
import { Paper } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { santoriniColor } from '../../style/color'
import { ItemPaperProps, itemProps } from './CovidContent'

const ContentBlock = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  border-radius: 5px;
  position: relative;
`

const ItemPaper = styled(Paper)<ItemPaperProps>`
  display: flex;
  flex: 1;
  height: 100%;
  width: 100%;
  margin-right: 1em;

  .item-header {
    flex: 2 auto;
    padding: 1em;

    .title {
      font-size: 12px;
      font-weight: 600;
      margin: 0;
      color: #a0aec0;
    }

    .number {
      font-size: 17px;
      font-weight: 700;
    }

    .rate {
      font-size: 12px;
    }
  }

  .item-main {
    flex: 1;
    margin: 0.7em;
    border-radius: 12px;
    background-color: ${(props) => props.main_bg_color};

    .image-wrap {
      padding: 0.5em;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    img {
      width: 32px;
      height: 32px;
      object-fit: cover;
    }
  }
`

export const SkeletonContentItem = () => {
  return (
    <ItemPaper elevation={1} main_bg_color={santoriniColor.santorini_blue}>
      <div className="item-header">
        <div className="title">
          <Skeleton variant="text" />
        </div>
        <div className="number">
          <Skeleton variant="text" />
        </div>
      </div>
      <Paper elevation={1} className="item-main">
        <div className="image-wrap">
          <Skeleton
            animation="wave"
            variant="rect"
            width={'100%'}
            height={'100%'}
          />
        </div>
      </Paper>
    </ItemPaper>
  )
}

const SkeletonCovidContent = () => {
  return (
    <ContentBlock>
      <SkeletonContentItem />
      <SkeletonContentItem />
      <SkeletonContentItem />
      <SkeletonContentItem />
    </ContentBlock>
  )
}
export default SkeletonCovidContent
