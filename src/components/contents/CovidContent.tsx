import { CovidContentProps } from '../../interfaces/CovidContent'
import styled from 'styled-components'
import { Paper } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { santoriniColor } from '../../style/color'
import { useEffect, useState } from 'react'
import { characterEntities } from 'character-entities'

export interface itemProps {
  item: CovidContentProps
}

export interface ItemPaperProps {
  main_bg_color: string
}

const ContentBlock = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  border-radius: 5px;
  position: relative;
`

const ItemPaper = styled(Paper)<ItemPaperProps>`
  display: flex;
  flex: 1;
  height: 100%;
  width: 100%;
  margin-right: 1em;

  .item-header {
    flex: 2 auto;
    padding: 1em;

    .title {
      font-size: 12px;
      font-weight: 600;
      margin: 0;
      color: #a0aec0;
    }

    .number {
      font-size: 17px;
      font-weight: 700;
    }

    .rate {
      font-size: 12px;
    }
  }

  .item-main {
    flex: 1;
    margin: 0.7em;
    border-radius: 12px;
    background-color: ${(props) => props.main_bg_color};
    //background-color: #3b72e8;
    //background-color: #3b72e8;

    .image-wrap {
      padding: 0.5em;
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    img {
      width: 32px;
      height: 32px;
      object-fit: cover;
    }
  }
`

const contentTitle = {
  todayConfirmed: '신규 확진자',
  todayDeaths: '신규 사망자',
  totalConfirmed: '총 확진자',
  totalDeaths: '총 사망자',
}

export const ContentItem = ({ figure, title, rate, icon }) => {
  console.log('figure ', figure)
  console.log('rate ', rate)
  return (
    <ItemPaper elevation={1} main_bg_color={santoriniColor.santorini_blue}>
      <div className="item-header">
        <div className="title">{title}</div>

        <div className="number">
          {figure}
          {rate > 0 && <span className="rate">({rate.toFixed(2)}%)</span>}
        </div>
      </div>
      <Paper elevation={1} className="item-main">
        <div className="image-wrap">
          {icon && <img src={`/images/icons/${icon}`} />}
          {!icon && (
            <Skeleton
              animation="wave"
              variant="rect"
              width={'100%'}
              height={'100%'}
            />
          )}
        </div>
      </Paper>
    </ItemPaper>
  )
}

export const SkeletonContentItem = () => {
  return (
    <ItemPaper elevation={1} main_bg_color={santoriniColor.santorini_blue}>
      <div className="item-header">
        <div className="title">
          <Skeleton variant="text" />
        </div>
        <div className="number">
          <Skeleton variant="text" />
        </div>
      </div>
      <Paper elevation={1} className="item-main">
        <div className="image-wrap">
          <Skeleton
            animation="wave"
            variant="rect"
            width={'100%'}
            height={'100%'}
          />
        </div>
      </Paper>
    </ItemPaper>
  )
}

const CovidContent = ({ item }: itemProps) => {
  console.log('CovidContent', item)
  const [{ todayConfirmed }, setTodayConfirmed] = useState(item)
  const [{ todayDeaths }, setTodayDeaths] = useState(item)
  const [{ deaths }, setDeaths] = useState(item.covidCountriesData.timeline[0])
  const [{ confirmed }, setConfirmed] = useState(
    item.covidCountriesData.timeline[0],
  )
  const [{ death_rate }, setDeath_rate] = useState(
    item.covidCountriesData.latest_data.calculated,
  )
  return (
    <ContentBlock>
      <ContentItem
        figure={todayConfirmed}
        rate={null}
        title={contentTitle.todayConfirmed}
        icon={'hospital.png'}
      />
      <ContentItem
        figure={todayDeaths}
        rate={null}
        title={contentTitle.todayDeaths}
        icon={'rip_white.png'}
      />
      <ContentItem
        figure={confirmed}
        rate={null}
        title={contentTitle.totalConfirmed}
        icon={'covid-19_white.png'}
      />
      <ContentItem
        figure={deaths}
        rate={death_rate}
        title={contentTitle.totalDeaths}
        icon={'rip_white.png'}
      />
    </ContentBlock>
  )
}
export default CovidContent
