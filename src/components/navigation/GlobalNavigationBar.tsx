import { BottomNavigation, BottomNavigationAction } from '@material-ui/core'
import Link from 'next/link'
import React, { ReactNode } from 'react'
import styled from 'styled-components'
import Login from '../account/Login'
import SearchButton from '../button/SearchButton'

type Props = {
  title?: string
}

const HeaderWrapper = styled.header`
  width: 100%;
  height: 80px;
  border-bottom: 1px solid #f0edf7;
`
const HeaderBlock = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: nowrap;
  height: 100%;
  align-items: center;
`

const NavWrapper = styled.div`
  display: flex;
  padding-left: 1.25em;
  flex: 1 0 140px !important;
  //font-size: 1.125rem;
  cursor: pointer;
  white-space: pre;
  text-decoration: none;
  color: inherit;

  &:hover {
    color: #495057;
  }

  &.active {
    font-weight: 600;
    border-bottom: 2px solid #22b8cf;
    color: #22b8cf;

    &:hover {
      color: #3bc9db;
    }
  }
`

const NavItem = styled.div`
  margin-left: 0.5em;
`

const LogoBlock = styled.div`
  flex: 0 auto;
  cursor: pointer;
`

const GlobalNavigationBar = ({ title = 'Next-Covid' }: Props) => {
  return (
    <>
      <HeaderWrapper>
        <HeaderBlock className="global-wrapper">
          <LogoBlock>
            <Link href="/">
              <h2>{title}</h2>
            </Link>
          </LogoBlock>
          <NavWrapper>
            <NavItem>
              <Link href="/news">
                <h2>코로나 소식</h2>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="/travel-course">
                <h2>여행 코스</h2>
              </Link>
            </NavItem>
          </NavWrapper>
          <SearchButton />
          <Login />
        </HeaderBlock>
      </HeaderWrapper>
    </>
  )
}

export default GlobalNavigationBar
