import Link from 'next/link'
import { StylesProvider } from '@material-ui/core/styles'
import styled from 'styled-components'
import Thumbnail from '../images/Thumbnail'
import ThumbnailSkeleton from '../skeleton/ThumbnailSkeleton'
import { Paper } from '@material-ui/core'

const InformationWrapper = styled(Paper)`
  display: flex;
  height: 12em;
  margin-top: 1.4em;
  padding: 1em;
  background-color: #3b72e8 !important;
  color: #f0edf7 !important;

  span {
    background-color: #f0edf7;
  }

  div.new_content {
    padding-left: 1em;
    display: flex;
    flex-direction: column;
  }

  &&:hover {
    //background-color: #f0edf7;
    cursor: pointer;
  }
`
const ImageBlock = styled.img`
  width: 12em;
  height: 100%;
`

const InformationLink = ({ name, img }) => {
  const wikiUrl = `https://ko.wikipedia.org/wiki/${name}`
  const googleUrl = `https://www.google.com/search?q=${name}`
  return (
    <StylesProvider injectFirst>
      <Link href={googleUrl}>
        <InformationWrapper>
          {img ? <Thumbnail name={name} img={img} /> : <ThumbnailSkeleton />}
          <div className="new_content">
            <h3>{name} 에 대해 궁금하다면?</h3>
          </div>
        </InformationWrapper>
      </Link>
    </StylesProvider>
  )
}

export default InformationLink
