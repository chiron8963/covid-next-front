import styled from 'styled-components'

const PageHistoryStepperWrapper = styled.div`
  padding: 1.5em;
  color: #3b72e8;
`

const PageHistoryStepper = ({ pageName, name }) => {
  return (
    <PageHistoryStepperWrapper>
      <h2>
        {pageName} - {name}
      </h2>
    </PageHistoryStepperWrapper>
  )
}

export default PageHistoryStepper
