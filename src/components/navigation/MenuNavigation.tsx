import styled from 'styled-components'
import { useRecoilState } from 'recoil'
import { pageHistoryState } from '../../recoil/history/pageHistoryState'
import { useEffect } from 'react'

export const MenuType = {
  NEWS: 'news',
  TRAVEL_COURSE: 'travel-course',
  COVID_COUNTRY: 'covid-country',
}

const MenuNavigationWrapper = styled.div``

const MenuNavigation = (props) => {
  useEffect(() => {
    const url = document.location.pathname
  }, [])

  return <MenuNavigationWrapper></MenuNavigationWrapper>
}

export default MenuNavigation
