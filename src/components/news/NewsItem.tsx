import { News } from '../../interfaces/News'
import styled from 'styled-components'
import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import { Paper } from '@material-ui/core'
import { StylesProvider } from '@material-ui/core/styles'
import dynamic from 'next/dynamic'
import React from 'react'

//const NewsBlock = styled.li`
const NewsBlock = styled(Paper)`
  display: flex;
  padding-bottom: 3em;
  padding: 1em;
  border-radius: 10px;
  margin-bottom: 1em;
  cursor: pointer;
  width: 768px;

  .thumbnail {
    margin-right: 1rem;

    img {
      display: block;
      width: 160px;
      height: 100px;
      object-fit: cover;
    }
  }

  .contents {
    h2 {
      margin: 0;
    }

    a {
      color: black;
    }

    p {
      margin: 0.5rem 0 0;
      line-height: 1.5;
      white-space: normal;
    }
  }

  & + & {
    margin-top: 3rem;
  }
`
const Thumbnail = styled.img``

interface NewsProps {
  news: News
}

const WysiwygViewer = dynamic(
  () => import('../../../src/components/editor/WysiwygViewer'),
  { ssr: false },
)

const NewsItem = ({ news: news }: NewsProps) => {
  return (
    <StylesProvider>
      <Link href="/news/[id]" as={`/news/${news.id}`}>
        <NewsBlock>
          {news.img && <Thumbnail />}
          <div className="contents">
            <h2>{news.title}</h2>
            {/*{news.content.length > 200 ? (*/}
            {/*  <WysiwygViewer*/}
            {/*    initialValue={`${news.content.substring(0, 200)}.....`}*/}
            {/*  />*/}
            {/*) : (*/}
            {/*  <WysiwygViewer initialValue={news.content} />*/}
            {/*)}*/}
          </div>
        </NewsBlock>
      </Link>
    </StylesProvider>
  )
}

export default NewsItem
