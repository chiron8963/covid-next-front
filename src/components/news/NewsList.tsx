import { News } from '../../interfaces/News'
import styled from 'styled-components'
import NewsItem from './NewsItem'
import ListItem from '../list/ListItem'
import * as React from 'react'

interface NewsProps {
  content: News[]
  type: string
}

const NewsListWrapper = styled.ul`
  margin: 0;
  padding: 0;
  flex: 1;
`

const NewsList = ({ content, type }: NewsProps) => {
  return (
    <NewsListWrapper>
      {type === 'NEWS' &&
        content.map((news) => <NewsItem key={news.id} news={news} />)}
      {type === 'SEARCH' &&
        content
          .filter((value, index) => index < 5)
          .map((news) => <NewsItem key={news.id} news={news} />)}
    </NewsListWrapper>
  )
}

export default NewsList
