import styled from 'styled-components'
import { StylesProvider } from '@material-ui/core/styles'
import { Paper } from '@material-ui/core'
import Router from 'next/router'
import Link from 'next/link'

const NewsLinkItemWrapper = styled(Paper)`
  display: flex;
  height: 12em;
  margin-top: 1.4em;
  padding: 1em;

  div.new_content {
    padding-left: 1em;
    display: flex;
    flex-direction: column;
  }

  &&:hover {
    background-color: #f0edf7;
    cursor: pointer;
  }
`
const ImageBlock = styled.img`
  width: 12em;
  height: 100%;
`

const NewsLinkItem = ({ name }) => {
  return (
    <StylesProvider injectFirst>
      <Link href="/news">
        <NewsLinkItemWrapper>
          <ImageBlock />
          <div className="new_content">
            <h3>{name} 코로나 관련 소식이 궁금하다면?</h3>
            <div>{name} 지역의 코로나 최신 안전소식 정보 확인하기..</div>
          </div>
        </NewsLinkItemWrapper>
      </Link>
    </StylesProvider>
  )
}

export default NewsLinkItem
