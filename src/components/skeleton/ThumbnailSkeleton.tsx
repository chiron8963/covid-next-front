import Skeleton from '@material-ui/lab/Skeleton'

const ThumbnailSkeleton = () => {
  return (
    <Skeleton animation="wave" variant="rect" width={'30%'} height={'100%'} />
  )
}

export default ThumbnailSkeleton
