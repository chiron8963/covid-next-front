import styled from 'styled-components'
import { Paper } from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton'

const ContentSkeletonWrapper = styled(Paper)`
  display: flex;
  height: 15em;
  width: 100%;
  padding: 1.25em;
  margin-bottom: 1.5em;
  //background-color: #f0edf7;

  &&:hover {
    background-color: #f0edf7;
    cursor: pointer;
  }
`
const DescriptionBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 1em;
  height: 100%;
`

const ContentSkeleton = () => {
  return (
    <ContentSkeletonWrapper>
      <Skeleton animation="wave" variant="rect" width={170} height={132} />
      <DescriptionBlock>
        <Skeleton animation="wave" width={450} />
        <Skeleton animation="wave" width={450} />
        <Skeleton animation="wave" width={450} />
        <Skeleton animation="wave" width={450} />
      </DescriptionBlock>
    </ContentSkeletonWrapper>
  )
}

export default ContentSkeleton
