import styled from 'styled-components'
import ThumbnailCard, { thumbNailType } from '../cards/ThumbnailCard'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { contentType } from '../../../pages/search'
import NewsList from '../news/NewsList'
import { log } from 'util'
import { Chip } from '@material-ui/core'
import { Avatar } from '@material-ui/core'
import Link from 'next/link'
import { useRecoilState, useRecoilValue } from 'recoil'
import { searchKeywordState } from '../../recoil/search/searchKeywordState'

const SearchResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-bottom: 2em;
  border-top: none;
  border-bottom: 1px solid #ededed;
  margin-left: auto;
  margin-right: auto;
  width: 52rem;

  .content-header {
    display: flex;

    .title {
      flex: 1;
      margin: 1em 0 0;
    }

    .count {
      color: #3b72e8;
      margin-left: 5px;
    }

    .sub {
      margin-top: 1em;

      h4 {
        margin: 0;
      }
    }
  }
`

const SearchResultBlock = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
  //margin-left: 4em;
  //margin-right: 4em;
  .result {
    margin: 1em;
  }
`

const SearchContent = ({ items, title, contentType }) => {
  const [contents, setContents] = useState([])
  const [size, setSize] = useState<number>(0)
  const [nextLink, setNextLink] = useState<string>(null)
  const keyword = useRecoilValue<string>(searchKeywordState)

  useEffect(() => {
    const { contents, size } = items
    setContents(contents)
    setSize(size)
    setNextLink(contentType.toLowerCase())
  }, [contents])

  return (
    <SearchResultWrapper>
      <div className="content-header">
        <h3 className="title">
          {title}
          <span className="count">{size}</span>
        </h3>
        {size > 5 && contentType !== 'COUNTRY' && (
          <Link
            href={{
              pathname: `/${nextLink}`,
              // query: { keyword: keyword },
            }}>
            <div className="sub">
              <h4>더보기</h4>
            </div>
          </Link>
        )}
      </div>
      <SearchResultBlock>
        {contentType === 'COUNTRY' && (
          <div className="result">
            {contents.map((content) => (
              <Link
                href={{
                  pathname: '/country/[name]',
                }}
                as={`/country/${content.countryNameEn}`}>
                <Chip
                  key={content.countryId}
                  avatar={<Avatar alt="Natacha" src={content.imgUrl} />}
                  label={content.countryNameKr}
                  variant="outlined"
                />
              </Link>
            ))}
          </div>
        )}
        {contentType === 'NEWS' && (
          <div className="result">
            <NewsList content={contents} type={'SEARCH'} />
          </div>
        )}
        {contentType === 'TRAVEL-COURSE' && (
          <>
            {contents.map((content) => (
              <ThumbnailCard
                key={content.travelCourseId}
                item={content}
                link={'/travel-course'}
                type={thumbNailType.TRAVEL}
              />
            ))}
          </>
        )}
      </SearchResultBlock>
    </SearchResultWrapper>
  )
}

export default SearchContent
