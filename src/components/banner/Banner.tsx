import styled from 'styled-components'
import React, { useState } from 'react'
import { useEffect } from 'react'
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

const BannerSwiperBlock = styled(Swiper)`
  width: 100%;
  height: 100%;
`

const BannerWrapper = styled.div`
  width: 100%;
  height: 400px;
  margin-top: 2.5em;
  border-radius: 5px;
`
const BannerBlock = styled.div`
  height: 100%;
  position: relative;

  .image-wrapper {
    width: 100%;
    height: 100%;

    img {
      width: 100%;
      height: 100%;
      border-radius: 7px;
    }
  }

  .content-wrapper {
    padding: 1em;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
    justify-content: center;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: 2;

    .content-title {
      color: #ffffff;
      font-weight: 700;
      font-size: 33px;
      line-height: 2.5;
    }
  }
`
const mockData = {
  data: [
    {
      id: 0,
      img: 'images/illustration/mountains-1412683_1280.png',
      title:
        '포스트 코로나 시대 이후 여행에 대한 Needs 해소를 목표로 하는 서비스 입니다. ',
    },
    // {
    //   id: 1,
    //   img: 'images/illustration/mountain-and-sea-4770131_1920.jpg',
    //   title:
    //     '포스트 코로나 시대 이후 여행에 대한 Needs 해소를 목표로 하는 서비스 입니다. ',
    // },
  ],
}
SwiperCore.use([Navigation, Pagination, Autoplay])
const Banner = () => {
  const [items, setItems] = useState([])
  useEffect(() => {
    setItems(mockData.data)
  }, [])

  return (
    <BannerWrapper>
      <BannerSwiperBlock
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}>
        {items &&
          items.map((item) => {
            return (
              <SwiperSlide key={item.id}>
                <BannerBlock>
                  <div className="image-wrapper">
                    <img src={item.img} />
                  </div>
                  <div className="content-wrapper">
                    <div className="content-title">{item.title}</div>
                  </div>
                </BannerBlock>
              </SwiperSlide>
            )
          })}
      </BannerSwiperBlock>
    </BannerWrapper>
  )
}

export default Banner
