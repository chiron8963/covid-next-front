import styled from 'styled-components'

interface ThumbnailProps {
  img: string
  name: string
}

const ThumbnailBlock = styled.img`
  width: 15em;
`

const Thumbnail = ({ img, name }: ThumbnailProps) => {
  return <ThumbnailBlock src={img} alt={name} />
}

export default Thumbnail
