import styled from 'styled-components'
import Skeleton from '@material-ui/lab/Skeleton'

const ContentImageWrapper = styled.div`
  display: flex;
  //width: 100%;
  width: 100%;
  height: 23em;
  margin-bottom: 2em;
`
const ImageBlock = styled.img`
  width: 100%;
`

const PageContentImage = () => {
  const img = null
  return (
    <ContentImageWrapper>
      {img ? (
        <ImageBlock />
      ) : (
        <Skeleton
          animation="wave"
          variant="rect"
          width={'100%'}
          height={'100%'}
        />
      )}
    </ContentImageWrapper>
  )
}

export default PageContentImage
