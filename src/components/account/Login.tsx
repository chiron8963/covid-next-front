import styled from 'styled-components'
import Modal from '@material-ui/core/Modal'
import { useState, ChangeEvent, useEffect } from 'react'
import CloseIcon from '@material-ui/icons/Close'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import { Button } from '@material-ui/core'
import Link from 'next/link'
import TextField from '@material-ui/core/TextField'
import { useRouter } from 'next/router'
import { useRecoilState } from 'recoil'
import { memberState } from '../../recoil/member'
import cookies from 'react-cookies'
import {
  ACCESS_TOKEN,
  REFRESH_TOKEN,
  GOOGLE_AUTH_URL,
  KAKAO_AUTH_URL,
} from '../../constants'
import Alert from '@material-ui/lab/Alert'
import Fade from '@material-ui/core/Fade'
import useUser from '../../lib/api/auth/useUser'
import { MemberLoginRequest } from '../../interfaces/MemberLoginRequest'
import { postLogin } from '../../lib/api/member/postLogin'
import { LOGIN_ERROR } from '../../constants/errorMassage'
import { delete_cookie } from '../utils/ResponseCookies'
import Avatar from '@material-ui/core/Avatar'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

interface IError {
  error: boolean
  message: string
}

const LoginButton = styled(Button)``
const LogoutButton = styled(Button)``

const ChevronLeftIconButton = styled(ChevronLeftIcon)`
  position: absolute;
  left: -8px;
  top: 5px;
  font-size: 27px !important;
  cursor: pointer;
`

const CloseButton = styled(CloseIcon)`
  cursor: pointer;
`
const ModalDiv = styled.div`
  position: absolute;
  width: 500px;
  height: 650px;
  background: white;
  border-radius: 5px;
  padding: 15px 40px 40px 40px;

  .mainView {
    margin-bottom: 20px;
  }

  .subView {
    margin-bottom: 80px;
  }

  .leftButtonViewOn {
    display: block;
  }

  .leftButtonViewOff {
    display: none;
  }
`

const ModalHeader = styled.div`
  text-align: right;
  padding: 10px;
  position: relative;
`
const ModalTitle = styled.h2`
  margin-top: 20px;
  font-size: 28px;
`
const MainGrid = styled.div``

const SubGrid = styled.div``

const GoogleGrid = styled.div`
  border: 1px solid black;
  width: 95%;
  height: 45px;
  border-radius: 10px;
  margin: 5px;
  text-align: center;
  position: relative;
  padding-top: 12px;
  cursor: pointer;

  &:before {
    content: ' ';
    background-image: url(/images/google_logo.png);
    background-size: 20px 20px;
    width: 20px;
    height: 20px;
    display: inline-block;
    position: absolute;
    top: 11px;
    left: 24px;
  }
`
const KakaoGrid = styled.div`
  border: 1px solid black;
  width: 95%;
  height: 45px;
  border-radius: 10px;
  margin: 5px;
  text-align: center;
  position: relative;
  padding-top: 12px;
  cursor: pointer;

  &:before {
    content: ' ';
    background-image: url(/images/kakao_logo.png);
    background-size: 25px 25px;
    width: 25px;
    height: 25px;
    display: inline-block;
    position: absolute;
    top: 9px;
    left: 22px;
  }
`
const EmailGrid = styled.div`
  border: 1px solid black;
  width: 95%;
  height: 45px;
  border-radius: 10px;
  margin: 5px;
  text-align: center;
  position: relative;
  padding-top: 12px;
  cursor: pointer;

  &:before {
    content: ' ';
    background-image: url('/images/mail_logo.png');
    background-size: 30px 30px;
    width: 30px;
    height: 30px;
    display: inline-block;
    position: absolute;
    top: 6px;
    left: 20px;
  }
`
const LogingB = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  height: 40px;
  margin-top: 10px !important;
`
const ModalFooter = styled.div`
  position: absolute;
  bottom: 35px;
  width: 410px;
`

const Singup = styled.div`
  text-align: center;
  margin-top: 15px;
`

const ErrorAlert = styled(Alert)`
  margin-top: 5px;
`

const AvatarLogin = styled.div`
  display: flex;
  padding-top: 3px;
  padding-left: 8px;
`
const LoginDiv = styled.div`
  min-width: 100px;
  display: flex;
  border-radius: 15px;
  border: 1px solid #adacac;
  color: #484848;
  font-weight: bold;
  cursor: pointer;
  height: 35px;
  padding: 5px;
`
const AvatarIcon = styled(Avatar)`
  width: 24px !important;
  height: 24px !important;
`

const Login = () => {
  const { loggedIn, user, mutate } = useUser()
  const [member, setMember] = useRecoilState(memberState)
  const [isEmailView, setIsEmailView] = useState<boolean>(false)
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [isOpen, setOpen] = useState<boolean>(false)
  const [isError, setError] = useState<IError>({
    error: false,
    message: '',
  })
  const [modalStyle] = useState(getModalStyle)
  const [state, setState] = useState({
    email: '',
    password: '',
  })
  const router = useRouter()
  const open = Boolean(anchorEl)
  function rand() {
    return Math.round(Math.random() * 20) - 10
  }

  function getModalStyle() {
    const top = 40 + rand()
    const left = 50

    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    }
  }

  const chanePopup = (): void => {
    if (isOpen) {
      setOpen(false)
      setIsEmailView(false)
    } else {
      setOpen(true)
    }

    setError({
      ...isError,
      error: false,
      message: '',
    })
  }

  const GoogleLogin = (): void => {
    location.href = GOOGLE_AUTH_URL
  }

  const KakaoLogin = (): void => {
    location.href = KAKAO_AUTH_URL
  }

  const emailLogin = (): void => {
    setIsEmailView(true)
  }

  const backLogin = (): void => {
    setIsEmailView(false)
  }

  // Login Input Change 핸들러 아이디 & 패스워드를 저장
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value

    setState(Object.assign(state, { [inputName]: inputValue }))
  }

  // Login 핸들러 토큰 cookie에 저장
  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault()
    const loginRequest: MemberLoginRequest = Object.assign({}, state)
    try {
      await postLogin(loginRequest)
      await mutate('/member/me')
      chanePopup()
    } catch (error) {
      setError({
        ...isError,
        error: true,
        message: LOGIN_ERROR,
      })
    }
  }

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  // logout 핸들러
  const handleLogout = async () => {
    cookies.remove(ACCESS_TOKEN)
    cookies.remove(REFRESH_TOKEN)
    delete_cookie(ACCESS_TOKEN, '/', null)
    setMember(null)
    await mutate('/member/me')
    await router.push('/')
  }

  const body = (
    <ModalDiv style={modalStyle}>
      <ModalHeader>
        <ChevronLeftIconButton
          onClick={backLogin}
          className={isEmailView ? 'leftButtonViewOn' : 'leftButtonViewOff'}
        />
        <CloseButton onClick={chanePopup} />
      </ModalHeader>
      <img src="/images/free-icon-passport-5507802.png" width="40"></img>
      <ModalTitle className={isEmailView ? 'mainView' : 'subView'}>
        어서 오세요. Covid-Next에 로그인 해주세요!
      </ModalTitle>
      {!isEmailView ? (
        <MainGrid>
          <GoogleGrid onClick={GoogleLogin}>구글로 로그인하기</GoogleGrid>
          <KakaoGrid onClick={KakaoLogin}>카카오로 로그인하기</KakaoGrid>
          <EmailGrid onClick={emailLogin}>이메일로 계속하기</EmailGrid>
        </MainGrid>
      ) : (
        <SubGrid>
          <>
            <form autoComplete="false" onSubmit={handleSubmit}>
              <TextField
                label="이메일 주소"
                placeholder="이메일 주소를 입력해주세요."
                fullWidth
                name="email"
                type="email"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                onChange={handleInputChange}
              />
              <TextField
                label="패스워드"
                placeholder="패스워드를 입력해주세요."
                fullWidth
                name="password"
                type="password"
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                onChange={handleInputChange}
              />
              <LogingB variant="contained" fullWidth type="submit">
                로그인
              </LogingB>
              <Fade in={isError.error}>
                <ErrorAlert variant="outlined" severity="error">
                  {isError.message.toString()}
                </ErrorAlert>
              </Fade>
              <Singup>
                <div>아직 회원가입을 하지 않으셨나요?</div>
                <div>
                  <Link href="/account/signupInfo" as={'/account/signupInfo'}>
                    <a>가입하여</a>
                  </Link>{' '}
                  Covid-Next를 최대한으로 활용하세요.
                </div>
              </Singup>
            </form>
          </>
        </SubGrid>
      )}
      <ModalFooter>
        계속 진행할 경우, Covid-Next의 개인정보 취급방침 및 쿠키 정책에 동의한
        것으로 간주됩니다.
      </ModalFooter>
    </ModalDiv>
  )
  if (loggedIn) {
    return (
      <div>
        <LoginDiv onClick={handleClick}>
          <AvatarIcon />
          <AvatarLogin>{member?.displayName}</AvatarLogin>
        </LoginDiv>
        <Menu
          id="fade-menu"
          MenuListProps={{
            'aria-labelledby': 'fade-button',
          }}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}>
          <Link href="/account/profile">
            <MenuItem onClick={handleClose}>개인정보 수정</MenuItem>
          </Link>
          <MenuItem onClick={handleLogout}>로그아웃</MenuItem>
        </Menu>
      </div>
    )
  } else {
    return (
      <div>
        <LoginButton onClick={chanePopup}>Login</LoginButton>
        <Modal open={isOpen} onClose={chanePopup}>
          {body}
        </Modal>
      </div>
    )
  }
}

export default Login
