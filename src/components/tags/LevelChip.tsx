import styled from 'styled-components'
import { StylesProvider } from '@material-ui/core/styles'
import React from 'react'

export interface ChipProp {
  customColor: string
  customLevel: number
}

const LevelChipBlock = styled.div<ChipProp>`
  height: 20px;
  text-align: center;
  margin-top: 5px;

  .levelSubBlock {
    display: inline-block;
    width: 40px;
    height: 4px;
    margin-right: 3px;
    background-color: #000000;
    opacity: 0.3;
  }

  .levelSubBlock:nth-child(-n + ${(props) => props.customLevel}) {
    background-color: ${(props) => props.customColor};
    opacity: 1;
  }
`
export const setColor = {
  1: '#2F80ED',
  2: '#27AE60',
  3: '#E2B93B',
  4: '#EB5757',
  99: '#EB5757',
}

const LevelChip = ({ level }) => {
  const color = setColor[level]
  return (
    <StylesProvider injectFirst>
      <>
        <LevelChipBlock customColor={setColor[level]} customLevel={level}>
          <div className="levelSubBlock" />
          <div className="levelSubBlock" />
          <div className="levelSubBlock" />
          <div className="levelSubBlock" />
        </LevelChipBlock>
      </>
    </StylesProvider>
  )
}

export default LevelChip
