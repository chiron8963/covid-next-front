import React, { ReactNode } from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { useRecoilValue } from 'recoil'
import Footer from './Footer'
import Header from './Header'
import styled from 'styled-components'

type Props = {
  children?: ReactNode
}

const LayoutBlock = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

const Layout = ({ children }: Props) => {
  return (
    <LayoutBlock>
      <Header />
      {/*<MainWrapper>*/}
      {children}
      {/*</MainWrapper>*/}
      <Footer />
    </LayoutBlock>
  )
}

export default Layout
