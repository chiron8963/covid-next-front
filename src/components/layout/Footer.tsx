import styled from 'styled-components'
import Link from 'next/link'

const FooterWrapper = styled.div`
  flex: 0;
  display: flex;
  margin-top: 25px;
  height: 20em;
  width: 100%;
  background-color: #fafafa;
  //border-top: 1px solid #f0edf7;

  .content {
    flex: 1;
    padding: 2em;
    display: flex;
    flex-direction: column;

    h4 {
      margin: 0;
      margin-bottom: 1em;
      cursor: pointer;
    }
  }
`

const Footer = () => {
  return (
    <FooterWrapper>
      <div className="global-wrapper content">
        <Link
          href={`https://eternalsunshine.notion.site/Release-note-a8b9779f65014bf4a29ce221a558ca88`}>
          <h4>Release note</h4>
        </Link>
        <Link
          href={`https://eternalsunshine.notion.site/Data-Source-15f7bfc5c9764ddebee30172b5c8dbd9`}>
          <h4>Data source</h4>
        </Link>
        <h4>Github</h4>
      </div>
    </FooterWrapper>
  )
}

export default Footer
