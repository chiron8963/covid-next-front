import styled from 'styled-components'

const SubHeaderBlock = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;

  h2.title {
    padding-top: 1em;
    width: 55%;
  }

  h2.country_info {
    width: 38%;
  }
`

const SubHeader = ({ title, infoType }) => {
  return (
    <SubHeaderBlock>
      <h2 className="title">{title}</h2>
      {infoType && <h2 className="country_info">{infoType}</h2>}
    </SubHeaderBlock>
  )
}

export default SubHeader
