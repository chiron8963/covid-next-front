import GlobalNavigationBar from '../navigation/GlobalNavigationBar'
import Head from 'next/head'
import React, { ReactNode } from 'react'
import styled from 'styled-components'

type Props = {
  title?: string
}
const HeaderWrapper = styled.div`
  flex: 0;
`

const Header = ({ title = 'Next Covid!' }: Props) => {
  return (
    <HeaderWrapper>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          http-equiv="Content-Security-Policy"
          content="upgrade-insecure-requests"
        />
      </Head>
      <GlobalNavigationBar />
    </HeaderWrapper>
  )
}

export default Header
