import {
  Editor as EditorType,
  EditorProps,
  Viewer as ViewerType,
  ViewerProps,
} from '@toast-ui/react-editor'
import { TuiViewWithForwardedProps } from './TuiViewWrapper'
import dynamic from 'next/dynamic'
import { forwardRef } from 'react'
import * as React from 'react'

interface EditorPropsWithHandlers extends ViewerProps {
  onChange?(value: string): void
}

const Viewer = dynamic<TuiViewWithForwardedProps>(
  () => import('./TuiViewWrapper'),
  { ssr: false },
)
const ViewWithForwardedRef = forwardRef<
  ViewerType | undefined,
  EditorPropsWithHandlers
>((props, ref) => (
  <Viewer {...props} forwardedRef={ref as React.MutableRefObject<ViewerType>} />
))

type Props = ViewerProps

const WysiwygViewer: React.FC<Props> = (props) => {
  const { initialValue } = props
  return (
    <div>
      <Viewer {...props} initialValue={initialValue || ''} />
    </div>
  )
}

export default WysiwygViewer
