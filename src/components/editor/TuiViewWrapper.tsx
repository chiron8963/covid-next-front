import React from 'react'
import { Viewer, ViewerProps } from '@toast-ui/react-editor'

export interface TuiViewWithForwardedProps extends ViewerProps {
  forwardedRef?: React.MutableRefObject<Viewer>
}

export default (props: TuiViewWithForwardedProps) => <Viewer {...props} />
