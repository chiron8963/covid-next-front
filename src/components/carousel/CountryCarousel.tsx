import styled from 'styled-components'
import React, { useEffect, useState } from 'react'
import CountryCardItem from '../cards/CountryCardItem'
import SwiperCore, { Pagination, Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'

interface DataProps {
  id: number
  title: string
  content: string
  imgUrl: string
  name: string
}

const SwiperWrapper = styled.div`
  width: 100%;

  .carousel-header {
    margin-bottom: 0;
  }
`
const SwiperBlock = styled(Swiper)`
  width: 100%;
  height: 30em;

  --swiper-navigation-color: #3b72e8;
  --swiper-navigation-size: 20px;

  .swiper-slide {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .swiper-button-prev {
    left: -3px;
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: white;
    box-shadow: 1px 1px 3px 0px #ababab;
  }

  .swiper-button-prev :hover {
    background-color: rgb(53, 197, 240);
    color: white !important;
  }

  .swiper-button-prev :after {
    font-size: 15px;
    font-weight: bold;
    padding-left: 2px;
  }

  .swiper-button-next {
    right: 3px;
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: white;
    box-shadow: 1px 1px 3px 0px #ababab;
  }

  .swiper-button-next :hover {
    background-color: rgb(53, 197, 240);
    color: white !important;
  }

  .swiper-button-next :after {
    font-size: 15px;
    font-weight: bold;
    padding-left: 2px;
  }
`

SwiperCore.use([Navigation, Pagination])

const CountryCarousel = ({ items, className, headerTitle }) => {
  const [data, setData] = useState<DataProps[]>([])
  useEffect(() => {
    setData(items.contents)
  }, [data])
  return (
    <SwiperWrapper>
      <h1 className="carousel-header">{headerTitle}</h1>
      <SwiperBlock spaceBetween={0} slidesPerView={4} navigation={true}>
        {data &&
          data.map((item) => {
            return (
              <SwiperSlide key={item.id}>
                <CountryCardItem item={item} />
              </SwiperSlide>
            )
          })}
      </SwiperBlock>
    </SwiperWrapper>
  )
}

export default CountryCarousel
