import { useState, ChangeEvent } from 'react'
import styled from 'styled-components'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import dynamic from 'next/dynamic'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox'
import { useRouter } from 'next/router'
import cookie from 'react-cookies'
import useUser from '../../src/lib/api/auth/useUser'
import { ACCESS_TOKEN } from '../../src/constants'
import { postSignupChange } from '../../src/lib/api/member/postSignupChange'
import Layout from '../../src/components/layout/Layout'
import MenuNavigation from '../../src/components/navigation/MenuNavigation'

interface ISignup {
  isTerms: boolean
}

interface IGender {
  gender: boolean
}

interface FieldPros {
  name: string
  checked: boolean
  value: string
}

const Signup = styled.div`
  text-align: center;
  background: white;
  padding: 25px;
  border: 1px solid #ddd;
  width: 1280px;
`
const SignupTerms = styled.div`
  text-align: center;
  background: white;
  padding: 25px;
  border: 1px solid #ddd;
`
const EmailInput = styled(TextField)`
  width: 75%;
  float: left;
`

const Gender = styled.div`
  width: 23%;
  float: left;
  margin-left: 10px;
  margin-top: 15px;

  .genderOn {
    border: 1px solid #112f70;
    color: #112f70;
    z-index: 1;
  }

  .genderOff {
    border: 1px solid #c4c4c4;
    color: #c4c4c4;
    z-index: 0;
  }
`
const GenderMen = styled.div`
  width: 50%;
  height: 56px;
  float: left;
  border-radius: 4px 0px 0px 4px;
  padding-top: 17px;
  cursor: pointer;
  position: relative;
`
const GenderWoman = styled.div`
  width: 50%;
  height: 56px;
  float: left;
  border-radius: 0px 4px 4px 0px;
  position: relative;
  left: -1px;
  padding-top: 17px;
  cursor: pointer;
  position: relative;
`
const PasswordInfo = styled.div`
  color: #6b6b6b;
  text-align: left;
`
const NextButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const SignupButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const Terms = styled.div`
  text-align: left;
  height: 300px;
  overflow-y: scroll;
`
const TermsTitle = styled.h3`
  text-align: left;
`
const AccountWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
const signup = () => {
  const [isGender, setIsGender] = useState<boolean>(true)
  const [state, setState] = useState({
    checkedA: false,
    checkedB: false,
  })
  const [sign, setSign] = useState({
    name: '',
    email: '',
    password: '',
    gender: '',
    phone: '',
  })
  const router = useRouter()
  const { loggedIn, user, mutate } = useUser()
  const changeGenderByMem = (): void => {
    setIsGender(true)
  }
  const changeGenderByWoman = (): void => {
    setIsGender(false)
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value
    setSign(Object.assign(sign, { [inputName]: inputValue }))
  }

  const handleSignup = async (event) => {
    event.preventDefault()

    Array.from(event.currentTarget.elements).forEach((field: FieldPros) => {
      if (field.name === 'gender' && field.checked) {
        setSign(Object.assign(sign, { [field.name]: field.value }))
      } else if (field.name != 'gender') {
        setSign(Object.assign(sign, { [field.name]: field.value }))
      }
    })

    const signupRequest = sign
    const accessToken = cookie.load(ACCESS_TOKEN)
    await postSignupChange(signupRequest, accessToken)
    router.push('/')
  }

  return (
    <Layout>
      <AccountWrapper>
        <MenuNavigation />
        <Signup className="global-wrapper">
          <form autoComplete="off" onSubmit={handleSignup}>
            <TermsTitle>회원정보</TermsTitle>
            <hr />
            <EmailInput
              label="이름"
              placeholder="이름을 입력해주세요"
              margin="normal"
              name="name"
              value={user?.name || ''}
              onChange={handleInputChange}
              InputLabelProps={{
                shrink: true,
              }}
              variant="filled"></EmailInput>
            <Gender>
              <input
                type="radio"
                name="gender"
                value="M"
                hidden
                defaultChecked={isGender}
                onChange={handleInputChange}></input>
              <input
                type="radio"
                name="gender"
                value="W"
                hidden
                defaultChecked={!isGender}
                onChange={handleInputChange}></input>
              <GenderMen className={isGender ? 'genderOn' : 'genderOff'}>
                남자
              </GenderMen>
              <GenderWoman className={!isGender ? 'genderOn' : 'genderOff'}>
                여자
              </GenderWoman>
            </Gender>
            <TextField
              label="이메일 주소"
              placeholder="이메일 형식으로 입력해주세요"
              fullWidth
              margin="normal"
              type="email"
              name="email"
              value={user?.email || ''}
              onChange={handleInputChange}
              InputLabelProps={{
                shrink: true,
              }}
              variant="filled"
            />
            <TextField
              label="별명"
              fullWidth
              required
              margin="normal"
              name="displayName"
              placeholder={user?.displayName || '별명을 입력해주세요'}
              onChange={handleInputChange}
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
            <SignupButton variant="contained" type="submit">
              변경하기
            </SignupButton>
          </form>
        </Signup>
      </AccountWrapper>
    </Layout>
  )
}

export default signup
