import { useState, ChangeEvent } from 'react'
import styled from 'styled-components'
import { Button } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import dynamic from 'next/dynamic'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox'
import { useRouter } from 'next/router'
import { postSignup } from '../../src/lib/api/member/postSignup'
import Layout from '../../src/components/layout/Layout'
import MenuNavigation from '../../src/components/navigation/MenuNavigation'

interface ISignup {
  isTerms: boolean
}

interface IGender {
  gender: boolean
}

const Signup = styled.div`
  text-align: center;
  background: white;
  padding: 25px;
  border: 1px solid #ddd;
  width: 1280px;
`
const SignupTerms = styled.div`
  text-align: center;
  background: white;
  padding: 25px;
  border: 1px solid #ddd;
  width: 1280px;
`
const EmailInput = styled(TextField)`
  width: 75%;
  float: left;
`

const Gender = styled.div`
  width: 23%;
  float: left;
  margin-left: 10px;
  margin-top: 15px;

  .genderOn {
    border: 1px solid #112f70;
    color: #112f70;
    z-index: 1;
  }

  .genderOff {
    border: 1px solid #c4c4c4;
    color: #c4c4c4;
    z-index: 0;
  }
`
const GenderMen = styled.div`
  width: 50%;
  height: 56px;
  float: left;
  border-radius: 4px 0px 0px 4px;
  padding-top: 17px;
  cursor: pointer;
  position: relative;
`
const GenderWoman = styled.div`
  width: 50%;
  height: 56px;
  float: left;
  border-radius: 0px 4px 4px 0px;
  position: relative;
  left: -1px;
  padding-top: 17px;
  cursor: pointer;
  position: relative;
`
const PasswordInfo = styled.div`
  color: #6b6b6b;
  text-align: left;
`
const NextButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const SignupButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const Terms = styled.div`
  text-align: left;
  height: 300px;
  overflow-y: scroll;
`
const TermsTitle = styled.h3`
  text-align: left;
`
const AccountWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const signup = () => {
  const [isGender, setIsGender] = useState<boolean>(true)
  const [isTerms, setIsTerms] = useState<boolean>(true)
  const [state, setState] = useState({
    checkedA: false,
    checkedB: false,
  })
  const [sign, setSign] = useState({
    name: '',
    email: '',
    password: '',
    gender: '',
    phone: '',
    displayName: '',
  })
  const TermsFirst = dynamic(() => import('../../src/components/account/terms'))
  const TermsSecond = dynamic(
    () => import('../../src/components/account/privacy'),
  )
  const router = useRouter()

  const changeTerms = () => {
    if (state.checkedA && state.checkedB) {
      setIsTerms ? setIsTerms(false) : setIsTerms(true)
    } else {
      alert('회원약관에 동의하셔야 가입이 가능합니다.')
    }
  }

  const changeGenderByMem = (): void => {
    setIsGender(true)
  }
  const changeGenderByWoman = (): void => {
    setIsGender(false)
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value

    setSign(Object.assign(sign, { [inputName]: inputValue }))
  }
  const handleSignup = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const signupRequest = Object.assign({}, sign)
    await postSignup(signupRequest)
    router.push('/')
  }
  return (
    <Layout>
      <AccountWrapper>
        <MenuNavigation />
        {isTerms ? (
          <SignupTerms className="global-wrapper">
            <TermsTitle>회원약관</TermsTitle>
            <hr />
            <Terms>
              <TermsFirst></TermsFirst>
            </Terms>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedA}
                    onChange={handleChange}
                    name="checkedA"
                    color="primary"
                    value=""
                  />
                }
                label="이용약관에 동의합니다.(필수)"
              />
            </FormGroup>

            <TermsTitle>개인정보 수집 및 이용 동의</TermsTitle>
            <hr />
            <Terms>
              <TermsSecond></TermsSecond>
            </Terms>

            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={state.checkedB}
                    onChange={handleChange}
                    name="checkedB"
                    color="primary"
                    value=""
                  />
                }
                label="개인정보 수집ㆍ이용에 동의합니다.(필수)"
              />
            </FormGroup>

            <NextButton variant="contained" onClick={changeTerms}>
              다음
            </NextButton>
          </SignupTerms>
        ) : (
          <Signup className="global-wrapper">
            <form autoComplete="off" onSubmit={handleSignup}>
              <TermsTitle>회원정보</TermsTitle>
              <hr />
              <EmailInput
                label="이름"
                placeholder="이름을 입력해주세요"
                margin="normal"
                name="name"
                onChange={handleInputChange}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"></EmailInput>
              <Gender>
                <input
                  type="radio"
                  name="gender"
                  value="M"
                  hidden
                  defaultChecked={isGender}
                  onChange={handleInputChange}></input>
                <input
                  type="radio"
                  name="gender"
                  value="W"
                  hidden
                  defaultChecked={!isGender}
                  onChange={handleInputChange}></input>
                <GenderMen
                  className={isGender ? 'genderOn' : 'genderOff'}
                  onClick={changeGenderByMem}>
                  남자
                </GenderMen>
                <GenderWoman
                  className={!isGender ? 'genderOn' : 'genderOff'}
                  onClick={changeGenderByWoman}>
                  여자
                </GenderWoman>
              </Gender>
              <TextField
                label="별명"
                placeholder="별명을 입력해주세요"
                fullWidth
                margin="normal"
                name="displayName"
                onChange={handleInputChange}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
              <TextField
                label="이메일 주소"
                placeholder="이메일 형식으로 입력해주세요"
                fullWidth
                margin="normal"
                type="email"
                name="email"
                onChange={handleInputChange}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />

              <TextField
                label="비밀번호"
                placeholder="비밀번호를 입력해주세요."
                fullWidth
                onChange={handleInputChange}
                type="password"
                margin="normal"
                name="password"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
              <PasswordInfo>
                8 - 16자리의 영문 대/소문자, 숫자, 특수문자 중 2개 이상을
                조합해서 비밀번호를 설정해 주세요.
              </PasswordInfo>
              <SignupButton variant="contained" type="submit">
                가입하기
              </SignupButton>
            </form>
          </Signup>
        )}
      </AccountWrapper>
    </Layout>
  )
}

export default signup
