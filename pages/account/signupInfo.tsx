import styled from 'styled-components'
import { Button } from '@material-ui/core'
import { useRouter } from 'next/router'
import Layout from '../../src/components/layout/Layout'
import { GOOGLE_AUTH_URL, KAKAO_AUTH_URL } from '../../src/constants'

const Signup = styled.div`
  text-align: center;
  width: 1280px;
`
const SignupTitle = styled.div`
  font-size: 40px;
  margin-top: 120px;
  font-weight: 600;
`
const SignupSubTitle = styled.div`
  color: #676767dd;
  margin-bottom: 60px;
`
const SignupBox = styled.div`
  width: 100%;
  height: 280px;
  border: 1px solid #ddd;
  margin-bottom: 30px;
  padding: 20px;
  padding-top: 50px;
  background: white;
`
const SignupLeft = styled.div`
  width: 50%;
  height: 90%;
  display: inline-block;
  float: left;
`
const SignupRight = styled.div`
  width: 50%;
  height: 90%;
  display: inline-block;
  float: left;
`

const SignupButton = styled(Button)`
  background: #3b72e8 !important;
  color: white !important;
  width: 90% !important;
  height: 50px;
  margin-top: 20px !important;
`

const OauthButton = styled(Button)`
  width: 90% !important;
  height: 50px;
  margin-top: 20px !important;
  background: white !important;
  position: relative;
`
const OauthImage = styled.img`
  position: absolute;
  left: 30px;
`
const AccountWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const signupInfo = () => {
  const router = useRouter()

  const GoogleLogin = (): void => {
    location.href = GOOGLE_AUTH_URL
  }

  const KakaoLogin = (): void => {
    location.href = KAKAO_AUTH_URL
  }
  const signup = (): void => {
    router.push('/account/signup')
  }

  return (
    <Layout>
      <AccountWrapper className="global-wrapper">
        <Signup>
          <SignupTitle>회원가입</SignupTitle>
          <SignupSubTitle>
            Covid-Next에 오신 것을 환영합니다 <br />
            회원이 되시면 더욱 다양한 혜택을 누리실 수 있습니다.
          </SignupSubTitle>
          <SignupBox>
            <SignupLeft>
              <h2>통합회원</h2>
              <SignupButton variant="contained" onClick={signup}>
                회원가입
              </SignupButton>
            </SignupLeft>
            <SignupRight>
              <h2>SNS 회원가입</h2>
              <OauthButton variant="contained" onClick={GoogleLogin}>
                <OauthImage src="/images/google_logo.png" width="25" />
                구글 아이디로 가입하기
              </OauthButton>
              <OauthButton variant="contained" onClick={KakaoLogin}>
                <OauthImage src="/images/kakao_logo.png" width="25" />
                카카오 아이디로 가입하기
              </OauthButton>
            </SignupRight>
          </SignupBox>
        </Signup>
      </AccountWrapper>
    </Layout>
  )
}

export default signupInfo
