import { GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'

import { User } from '../../src/interfaces'
import { sampleUserData } from '../../src/lib/utils/sample-data'
import Layout from '../../src/components/layout/Layout'
import ListDetail from '../../src/components/list/ListDetail'
import { getCities } from '../../src/lib/api/city/getCities'
import { useEffect } from 'react'
import styled from 'styled-components'
import * as React from 'react'
import PageContentImage from '../../src/components/images/PageContentImage'
import CityList from '../../src/components/cities/CityList'
import InformationLink from '../../src/components/navigation/InformationLink'
import SubHeader from '../../src/components/layout/SubHeader'

const CityWrapper = styled.div``

const ContentsWrapper = styled.div`
  display: flex;
`
const CityInfoWrapper = styled.div`
  width: 40%;
  padding: 1em 0 0 1em;
`

const CityDetail = ({ cities, cityName }) => {
  const pageName = '여행코스'
  return (
    <Layout>
      <CityWrapper className="global-wrapper">
        <PageContentImage />
        <SubHeader title={cityName} infoType={null} />
        <ContentsWrapper>
          <CityList cities={cities} countryName={cityName} />
          <CityInfoWrapper>
            <InformationLink name={cityName} img={null} />
          </CityInfoWrapper>
        </ContentsWrapper>
      </CityWrapper>
    </Layout>
  )
}

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<any> => {
  const name = params?.name
  console.log(name)

  try {
    // const res = await getCities(name)
    const res = null
    console.log(res)
    return { props: { cities: res, cityName: name } }
  } catch (err) {
    return { props: { errors: err.message, countries: null } }
  }
}

export default CityDetail
