import Link from 'next/link'
import Layout from '../src/components/layout/Layout'
import { useRecoilValue } from 'recoil'

const AboutPage = () => {
  return (
    <Layout>
      <h1>About</h1>
      <div>This is the about page</div>
      <div>
        <Link href="/">
          <a>Go home</a>
        </Link>
      </div>
    </Layout>
  )
}

export default AboutPage
