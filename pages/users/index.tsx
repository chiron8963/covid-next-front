import { GetStaticProps } from 'next'
import Link from 'next/link'
import { User } from '../../src/interfaces'
import { sampleUserData } from '../../src/lib/utils/sample-data'
import Layout from '../../src/components/layout/Layout'
import List from '../../src/components/list/List'

type Props = {
  items: User[]
}

const WithStaticProps = ({ items }: Props) => {
  return (
    <Layout>
      <h1> Users List </h1>
      <div>
        {' '}
        Example fetching data from inside
        <code>getStaticProps()</code>.
      </div>
      <div>You are currently on: /users</div>
      <List items={items} />
      <div>
        <Link href="/">
          <a>Go home</a>
        </Link>
      </div>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  // Example for including static props in a Next.js function component page.
  // Don't forget to include the respective types for any props passed into
  // the component.
  const items: User[] = sampleUserData
  return { props: { items } }
}

export default WithStaticProps
