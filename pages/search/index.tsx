import Layout from '../../src/components/layout/Layout'
import styled from 'styled-components'
import TextField from '@material-ui/core/TextField'
import { useEffect, useRef, useState } from 'react'
import { getTravelCourseSearch } from '../../src/lib/api/travelCourse/getTravelCourseSearch'
import ThumbnailCard, {
  thumbNailType,
} from '../../src/components/cards/ThumbnailCard'
import * as React from 'react'
import { withRouter } from 'next/router'
import { log } from 'util'
import SearchContent from '../../src/components/search/SearchContent'
import { getSearchAll } from '../../src/lib/api/search/getSearchAll'
import { useRecoilState } from 'recoil'
import { searchKeywordState } from '../../src/recoil/search/searchKeywordState'

const SearchWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  width: 100%;
`

const SearchInputWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin: 2.5em;
  padding-right: 7rem;
`
const SearchInputBlock = styled(TextField)`
  width: 50rem;
`

interface ItemProps {
  content: []
  size: number
}

export const contentType = {
  COUNTRY: 'COUNTRY',
  NEWS: 'NEWS',
  TRAVEL: 'TRAVEL-COURSE',
}

const SearchPage = ({ router: { query } }) => {
  const [keyword, setKeyWord] = useRecoilState(searchKeywordState)
  const [items, setItems] = useState(null)
  const [inputStatus, setInputStatus] = useState(true)
  const handleSubmit = (e) => {
    e.preventDefault()
    search()
  }

  const handleChangeInput = (e) => {
    const { value, name } = e.target
    setKeyWord(value)
  }

  const search = async () => {
    setItems(null)
    if (keyword.length > 0) {
      const res = await getSearchAll(keyword)
      setItems(res)
      setInputStatus(false)
    }
  }

  useEffect(() => {
    console.log(Object.is(query?.keyword, undefined))
  }, [items])

  return (
    <Layout>
      <SearchWrapper className="global-wrapper">
        <SearchInputWrapper>
          <form onSubmit={handleSubmit}>
            <SearchInputBlock
              label="Search"
              variant="outlined"
              id="SearchValue"
              size="medium"
              helperText={
                inputStatus
                  ? `검색어를 입력해주세요`
                  : `'${keyword}' 에 대한 통합 검색 결과`
              }
              onChange={handleChangeInput}
            />
          </form>
        </SearchInputWrapper>
        {items && (
          <>
            <SearchContent
              items={items.countries}
              title={'국가'}
              contentType={contentType['COUNTRY']}
            />
            <SearchContent
              items={items.covidNews}
              title={'관련 정보'}
              contentType={contentType['NEWS']}
            />
            {/*<SearchContent items={items} title={'도시'} contentType={null} />*/}
            <SearchContent
              items={items.travelCourse}
              title={'여행 코스'}
              contentType={contentType.TRAVEL}
            />
          </>
        )}
      </SearchWrapper>
    </Layout>
  )
}

export default withRouter(SearchPage)
