import dynamic from 'next/dynamic'
import Layout from '../../../src/components/layout/Layout'
import styled from 'styled-components'
import { useState, ChangeEvent, useRef } from 'react'
import MenuNavigation from '../../../src/components/navigation/MenuNavigation'
import React from 'react'
import TextField from '@material-ui/core/TextField'
import {
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText,
} from '@material-ui/core'
import useUser from '../../../src/lib/api/auth/useUser'
import { GetStaticProps } from 'next'
import { getCountry } from '../../../src/lib/api/covid/getCountry'
import { getCities } from '../../../src/lib/api/city/getCities'
import { useRouter } from 'next/router'
import '@toast-ui/editor/dist/toastui-editor.css'
import { TuiEditorWithForwardedProps } from '../../../src/components/editor/TuiEditorWrapper'
import { Editor as EditorType, EditorProps } from '@toast-ui/react-editor'
import { postTravelCourse } from '../../../src/lib/api/travelCourse/postTravelCourse'
import { useRecoilState } from 'recoil'
import { memberState } from '../../../src/recoil/member'
import Modal from '@material-ui/core/Modal'
import { City } from '../../../src/interfaces/City'
import { CityRequest } from '../../../src/interfaces/CityRequest'
import { postCity } from '../../../src/lib/api/city/postCity'

const CourseInput = styled(TextField)`
  width: 100%;
`
const SelectFormControl = styled(FormControl)`
  width: 30%;
  min-width: 220px !important;
  margin-bottom: 10px !important;
  margin-right: 10px !important;
`
const TravelCourseWrapper = styled.div``
const SubmitButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const ModalDiv = styled.div`
  position: absolute;
  width: 500px;
  height: 120px;
  background: white;
  border-radius: 5px;
  padding: 15px 40px 40px 40px;
`

const CityTextField = styled(TextField)`
  width: 80%;
`

const CountryAddButton = styled(Button)`
  background: #4b4b4b !important;
  color: white !important;
  position: relative;
  top: 16px;
  left: 10px;
  height: 56px;
`

const CityButton = styled(Button)`
  background: #4b4b4b !important;
  color: white !important;
  height: 40px;
  position: relative;
  top: 6px;
`
interface EditorPropsWithHandlers extends EditorProps {
  onChange?(value: string): void
}

interface CityProps {
  cityId: number
  cityNameKr: string
}

const Editor = dynamic<TuiEditorWithForwardedProps>(
  () => import('../../../src/components/editor/TuiEditorWrapper'),
  { ssr: false },
)
const EditorWithForwardedRef = React.forwardRef<
  EditorType | undefined,
  EditorPropsWithHandlers
>((props, ref) => (
  <Editor {...props} forwardedRef={ref as React.MutableRefObject<EditorType>} />
))

const TravelCourseInputPage = (country) => {
  const router = useRouter()
  const [isOpen, setOpen] = useState<boolean>(false)
  const [member, setMember] = useRecoilState(memberState)
  const [city, setCity] = useState<CityRequest>({
    countryId: 0,
    cityNameKr: '',
    cityNameEn: '',
  })
  const [course, setCourse] = useState({
    courseName: '',
    content: '',
    cityId: 0,
    userId: '',
  })

  const [cityItem, setCityItem] = useState(Array)
  const editorRef = useRef<EditorType>()
  const [modalStyle] = useState(getModalStyle)

  function rand() {
    return Math.round(Math.random() * 20) - 10
  }

  function getModalStyle() {
    const top = 40 + rand()
    const left = 50

    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    }
  }

  const handleCourse = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (!editorRef.current) {
      return
    }
    const instance = editorRef.current.getInstance()
    setCourse(Object.assign(course, { content: instance.getHTML() }))
    setCourse(Object.assign(course, { userId: member.id }))
    await postTravelCourse(course)
    router.push('/travel-course')
  }

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value

    setCourse(Object.assign(course, { [inputName]: inputValue }))
  }

  const handleInputCityChange = (
    event: ChangeEvent<HTMLInputElement>,
  ): void => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value
    setCity(Object.assign(city, { [inputName]: inputValue }))
  }

  const handleCountryChange = async (event: ChangeEvent<HTMLInputElement>) => {
    const target = event.target
    const inputValue = target.value
    await getCityItem(inputValue)
    setCity(Object.assign(city, { countryId: inputValue }))
  }

  const getCityItem = async (inputValue) => {
    const cityI = await getCities(inputValue)
    setCityItem(cityI)
  }

  const handleCityChange = async (event: ChangeEvent<HTMLInputElement>) => {
    const target = event.target
    const inputName = target.name
    const inputValue = target.value
    setCourse(Object.assign(course, { [inputName]: inputValue }))
  }
  const addCitySubmit = async () => {
    try {
      await postCity(city)
      setOpen(false)
      await getCityItem(city.countryId)
      alert('도시 이름이 추가되었습니다.')
    } catch (error) {
      alert('도시 이름 저장중 오류가 발생했습니다.')
    }
  }
  const changePopup = (): void => {
    if (city.countryId == 0) {
      alert('나라를 선택해주세요')
      return
    }

    if (isOpen) {
      setOpen(false)
    } else {
      setOpen(true)
    }
  }
  const body = (
    <ModalDiv style={modalStyle}>
      <CityTextField
        label="도시명"
        placeholder="도시명을 입력해주세요"
        margin="normal"
        name="cityNameKr"
        onChange={handleInputCityChange}
        InputLabelProps={{
          shrink: true,
        }}
        variant="outlined"
      />
      <CountryAddButton onClick={addCitySubmit}>도시 추가</CountryAddButton>
    </ModalDiv>
  )
  return (
    <Layout>
      <MenuNavigation />
      <TravelCourseWrapper className="global-wrapper">
        <form onSubmit={handleCourse}>
          <h1>여행지 정보 글쓰기</h1>
          <hr />
          <div>
            <CourseInput
              label="투어 이름"
              placeholder="투어이름을 입력해주세요"
              margin="normal"
              name="courseName"
              InputLabelProps={{
                shrink: true,
              }}
              required
              onChange={handleInputChange}
              variant="outlined"></CourseInput>
          </div>
          <div>
            <SelectFormControl variant="outlined">
              <InputLabel id="demo-simple-select-outlined-label">
                나라
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="country"
                name="countryId"
                onChange={handleCountryChange}
                required
                label="나라">
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {country.country &&
                  country.country.content.map((item, index) => (
                    <MenuItem value={item.countryId || ''} key={index}>
                      <em key={index}>{item.countryNameKr || ''}</em>
                    </MenuItem>
                  ))}
              </Select>
            </SelectFormControl>
            <SelectFormControl variant="outlined">
              <InputLabel id="demo-simple-select-outlined-label">
                도시
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="city"
                name="cityId"
                required
                onChange={handleCityChange}
                label="도시">
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {cityItem &&
                  cityItem.map((item: CityProps, index) => (
                    <MenuItem value={item.cityId || ''} key={index}>
                      <em key={index}>{item.cityNameKr || ''}</em>
                    </MenuItem>
                  ))}
              </Select>
            </SelectFormControl>
            <CityButton onClick={changePopup}>도시 추가</CityButton>
          </div>
          <div>
            <EditorWithForwardedRef
              previewStyle={'vertical'}
              height={'600px'}
              initialEditType={'wysiwyg'}
              useCommandShortcut={true}
              ref={editorRef}
            />
          </div>
          <div>
            <SubmitButton variant="contained" type="submit">
              저장
            </SubmitButton>
          </div>
        </form>
        <Modal open={isOpen} onClose={changePopup}>
          {body}
        </Modal>
      </TravelCourseWrapper>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async ({
  params,
}): Promise<any> => {
  try {
    const res = await getCountry()

    return { props: { country: res } }
  } catch (err) {
    return { props: { errors: err.message } }
  }
}

export default TravelCourseInputPage
