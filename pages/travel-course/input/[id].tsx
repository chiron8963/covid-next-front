import { GetServerSideProps } from 'next'
import MenuNavigation from '../../../src/components/navigation/MenuNavigation'
import Layout from '../../../src/components/layout/Layout'
import styled from 'styled-components'
import { getCourseDetail } from '../../../src/lib/api/travelCourse/getCourseDetail'
import '@toast-ui/editor/dist/toastui-editor-viewer.css'
import dynamic from 'next/dynamic'
import React, { useRef, useState } from 'react'
import { useRouter } from 'next/router'
import { Button } from '@material-ui/core'
import '@toast-ui/editor/dist/toastui-editor.css'
import { TuiEditorWithForwardedProps } from '../../../src/components/editor/TuiEditorWrapper'
import { Editor as EditorType, EditorProps } from '@toast-ui/react-editor'
import { putTravelCourse } from '../../../src/lib/api/travelCourse/putTravelCourse'
import useUser from '../../../src/lib/api/auth/useUser'
import { useRecoilState } from 'recoil'
import { memberState } from '../../../src/recoil/member'

interface EditorPropsWithHandlers extends EditorProps {
  onChange?(value: string): void
}

interface CityProps {
  cityId: number
  cityNameKr: string
}

const Editor = dynamic<TuiEditorWithForwardedProps>(
  () => import('../../../src/components/editor/TuiEditorWrapper'),
  { ssr: false },
)
const EditorWithForwardedRef = React.forwardRef<
  EditorType | undefined,
  EditorPropsWithHandlers
>((props, ref) => (
  <Editor {...props} forwardedRef={ref as React.MutableRefObject<EditorType>} />
))

const TravelCourseWrapper = styled.div`
  width: 1280px;
`
const TravelTitleDiv = styled.div`
  position: relative;
`

const TravelSubTitle = styled.div`
  font-size: 14px;
  font-style: italic;
  margin-top: 10px;
  color: dimgray;
`
const TravelBottom = styled.div`
  color: dimgray;
  border-top: 1px solid dimgray;
  border-bottom: 1px solid lightgray;
  padding: 5px;
  font-size: 8px;
`
const TravelTitle = styled.div`
  font-weight: 700;
  padding: 5px;
  font-size: 3em;
`

const TravelListButton = styled.button`
  min-width: 46px;
  height: 36px;
  margin-left: 10px;
  padding: 0 12px;
  font-size: 13px;
  line-height: 36px;
  border: 1px solid #00000000;
  background: #eff0f2;
  color: #000000;
  font-weight: 700;
  border-radius: 8px;
  float: right;
  margin-top: 15px;
  cursor: pointer;
`

const TravelEditButton = styled.button`
  min-width: 46px;
  height: 36px;
  margin-left: 10px;
  padding: 0 12px;
  font-size: 13px;
  line-height: 36px;
  border: 1px solid #00000000;
  background: #05276e;
  color: #ffffff;
  font-weight: 700;
  border-radius: 8px;
  float: right;
  margin-top: 15px;
  cursor: pointer;
`
const SubmitButton = styled(Button)`
  background: #112f70 !important;
  color: white !important;
  margin-top: 50px !important;
  width: 60%;
  height: 45px;
  border-radius: 15px;
`
const inputTravel = ({ course }) => {
  const router = useRouter()
  const editorRef = useRef<EditorType>()
  const [member, setMember] = useRecoilState(memberState)
  const [newCourse, setNewCourse] = useState({
    travelCourseId: 0,
    courseName: '',
    content: '',
    cityId: 0,
    userId: '',
  })
  const handleCourse = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    if (!editorRef.current) {
      return
    }
    setNewCourse(
      Object.assign(newCourse, {
        travelCourseId: course.id,
        content: editorRef.current.getInstance().getHTML(),
        userId: member.id,
        cityId: course.cityId,
        courseName: course.courseName,
      }),
    )

    await putTravelCourse(newCourse)
    router.push('/travel-course')
  }

  return (
    <Layout>
      <MenuNavigation />
      <TravelCourseWrapper className="global-wrapper">
        <TravelTitleDiv>
          <TravelSubTitle>
            {course.countryName} / {course.cityName}
          </TravelSubTitle>
          <TravelTitle>{course.courseName}</TravelTitle>
          <TravelBottom>
            {course.displayName} / {course.createdAt}
          </TravelBottom>
        </TravelTitleDiv>
        <form onSubmit={handleCourse}>
          <div>
            <EditorWithForwardedRef
              previewStyle={'vertical'}
              height={'600px'}
              initialEditType={'wysiwyg'}
              useCommandShortcut={true}
              ref={editorRef}
              initialValue={course.content}
            />
          </div>
          <div>
            <SubmitButton variant="contained" type="submit">
              저장
            </SubmitButton>
          </div>
        </form>
      </TravelCourseWrapper>
    </Layout>
  )
}

export default inputTravel

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<any> => {
  try {
    const id = params?.id
    const res = await getCourseDetail(id)
    return { props: { course: res } }
  } catch (err) {
    return { props: { errors: err.message, course: null } }
  }
}
