import { GetServerSideProps } from 'next'
import MenuNavigation from '../../src/components/navigation/MenuNavigation'
import Layout from '../../src/components/layout/Layout'
import styled from 'styled-components'
import { getCourseDetail } from '../../src/lib/api/travelCourse/getCourseDetail'
import '@toast-ui/editor/dist/toastui-editor-viewer.css'
import dynamic from 'next/dynamic'
import React from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import useUser from '../../src/lib/api/auth/useUser'
import { useRecoilState } from 'recoil'
import { memberState } from '../../src/recoil/member'
import { deleteTravelCourse } from '../../src/lib/api/travelCourse/deleteTravelCourse'

const WysiwygViewer = dynamic(
  () => import('../../src/components/editor/WysiwygViewer'),
  { ssr: false },
)

const TravelCourseWrapper = styled.div`
  width: 100%;
`
const TravelTitleDiv = styled.div`
  position: relative;
`

const TravelSubTitle = styled.div`
  font-size: 14px;
  font-style: italic;
  margin-top: 10px;
  color: dimgray;
`
const TravelBottom = styled.div`
  margin-bottom: 20px;
  color: dimgray;
  border-top: 1px solid dimgray;
  border-bottom: 1px solid lightgray;
  padding: 5px;
  font-size: 8px;
`
const TravelTitle = styled.div`
  font-weight: 700;
  padding: 5px;
  font-size: 3em;
`

const TravelListButton = styled.button`
  min-width: 46px;
  height: 36px;
  margin-left: 10px;
  padding: 0 12px;
  font-size: 13px;
  line-height: 36px;
  border: 1px solid #00000000;
  background: #eff0f2;
  color: #000000;
  font-weight: 700;
  border-radius: 8px;
  float: right;
  margin-top: 15px;
  cursor: pointer;
`

const TravelEditButton = styled.button`
  min-width: 46px;
  height: 36px;
  margin-left: 10px;
  padding: 0 12px;
  font-size: 13px;
  line-height: 36px;
  border: 1px solid #00000000;
  background: #05276e;
  color: #ffffff;
  font-weight: 700;
  border-radius: 8px;
  float: right;
  margin-top: 15px;
  cursor: pointer;
`
const TravelDelButton = styled.button`
  min-width: 46px;
  height: 36px;
  margin-left: 10px;
  padding: 0 12px;
  font-size: 13px;
  line-height: 36px;
  border: 1px solid #00000000;
  background: #8d1515;
  color: #ffffff;
  font-weight: 700;
  border-radius: 8px;
  float: right;
  margin-top: 15px;
  cursor: pointer;
`

const StaticPropsDetail = ({ course }) => {
  const router = useRouter()
  const [member, setMember] = useRecoilState(memberState)
  const handlerTravelListButton = () => {
    router.push('/travel-course')
  }

  const handlerTravelDelButton = async () => {
    await deleteTravelCourse(course)
    router.push('/travel-course')
  }
  console.log(course)
  return (
    <Layout>
      <MenuNavigation />
      <TravelCourseWrapper className="global-wrapper">
        <TravelTitleDiv>
          <TravelSubTitle>
            {course.countryName} / {course.cityName}
          </TravelSubTitle>
          <TravelTitle>{course.courseName}</TravelTitle>
          <TravelBottom>
            {course.displayName} / {course.createdAt}
          </TravelBottom>
        </TravelTitleDiv>
        <WysiwygViewer initialValue={course.content} />
        <TravelListButton onClick={handlerTravelListButton}>
          목록
        </TravelListButton>
        {course?.userId == member?.id ? (
          <span>
            <Link
              href={`/travel-course/input/[id]`}
              as={`/travel-course/input/${course.id}`}>
              <TravelEditButton>수정</TravelEditButton>
            </Link>
            <TravelDelButton onClick={handlerTravelDelButton}>
              삭제
            </TravelDelButton>
          </span>
        ) : null}
      </TravelCourseWrapper>
    </Layout>
  )
}

export default StaticPropsDetail

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<any> => {
  try {
    const id = params?.id
    const res = await getCourseDetail(id)
    return { props: { course: res } }
  } catch (err) {
    return { props: { errors: err.message, course: null } }
  }
}
