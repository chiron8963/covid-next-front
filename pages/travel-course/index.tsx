import { GetServerSideProps, GetStaticProps } from 'next'
import Layout from '../../src/components/layout/Layout'
import styled from 'styled-components'
import MenuNavigation from '../../src/components/navigation/MenuNavigation'
import Link from 'next/link'
import React, { useCallback, useEffect, useState } from 'react'
import { Card, Typography } from '@material-ui/core'
import { getCourse } from '../../src/lib/api/travelCourse/getCourse'
import useUser from '../../src/lib/api/auth/useUser'
import ThumbnailCard from '../../src/components/cards/ThumbnailCard'
import { thumbNailType } from '../../src/components/cards/ThumbnailCard'
import { useInView } from 'react-intersection-observer'
import CircularProgress from '@material-ui/core/CircularProgress'
import { getNews } from '../../src/lib/api/news/getNews'
import { Pageing } from '../../src/interfaces/Pageing'

const TravelCourseWrapper = styled.div`
  flex: 1;
  width: 100%;

  .Target-Element {
    display: flex;
    justify-content: center;
  }
`
const MenuHeader = styled.div`
  font-size: 20px;
  margin: 20px;
  font-weight: bold;
  position: relative;
`
const AddButton = styled.button`
  background: #112f70;
  color: white;
  font-weight: bold;
  position: absolute;
  right: 0px;
  padding: 5px;
  width: 130px;
  height: 30px;
  border: none;
  border-radius: 10px;

  &:hover {
    background: #0b2252;
  }
`
const CourseCardTopDiv = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-content: center;
  margin-top: 16px;
`

const pageing: Pageing = { page: 0, size: 20, sort: 'createdAt' }

const TravelCoursePage = ({ items }) => {
  const { loggedIn, user, mutate } = useUser()
  const [ref, inView] = useInView()
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [sizeChecking, setSizeChecking] = useState(false)

  const [courseItem, setCourseItem] = useState([])
  const getItems = useCallback(async () => {
    await setLoading(true)
    const { contents, size } = await getCourse(page, pageing.size, pageing.sort)

    await setCourseItem((prevState) => [...prevState, ...contents])
    size < 20 ? setSizeChecking(true) : setSizeChecking(false)
    await setLoading(false)
  }, [page])

  // //새로운 page 값이 있을때 call
  useEffect(() => {
    getItems()
  }, [getItems])

  //무한스크롤 하단 체크용
  useEffect(() => {
    if (inView && !loading && !sizeChecking) {
      setPage((prevState) => prevState + 1)
    }
  }, [inView, loading])
  return (
    <Layout>
      <MenuNavigation />
      <TravelCourseWrapper className="global-wrapper">
        <MenuHeader>
          여행코스
          {loggedIn ? (
            <Link
              href="/travel-course/input/input"
              as="/travel-course/input/input">
              <AddButton>코스등록</AddButton>
            </Link>
          ) : null}
        </MenuHeader>

        <CourseCardTopDiv>
          {courseItem &&
            courseItem.map((item) => (
              <ThumbnailCard
                key={item.id}
                item={item}
                link={'/travel-course'}
                type={thumbNailType.TRAVEL}
              />
            ))}
        </CourseCardTopDiv>
        <div ref={ref} className="Target-Element">
          {/*<CircularProgress />*/}
          {loading && <CircularProgress />}
        </div>
      </TravelCourseWrapper>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async ({
  params,
}): Promise<any> => {
  try {
    const res = null
    return { props: { items: res } }
  } catch (err) {
    return { props: { errors: err.message } }
  }
}

export default TravelCoursePage
