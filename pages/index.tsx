import Layout from '../src/components/layout/Layout'
import styled from 'styled-components'
import MainContent from '../src/components/contents/MainContent'
import { GetServerSideProps, GetStaticProps } from 'next'
import { getCovidContinentData } from '../src/lib/api/country/getCovidCountriesData'
import usePromise from '../src/hooks/usePromise'
import { CircularProgress } from '@material-ui/core'
import { useEffect } from 'react'

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;

  .main_item {
    flex: 1 1 0;
    display: flex;
  }

  .progress_position {
    margin: auto;
  }
`

const IndexPage = ({}) => {
  const [loading, response, error] = usePromise(() => {
    return getCovidContinentData()
  }, [])

  return (
    <Layout>
      <MainWrapper className="global-wrapper">
        {loading && (
          <div className="main_item">
            <CircularProgress color="inherit" className="progress_position" />
          </div>
        )}
        {!loading && response && <MainContent items={response} />}
      </MainWrapper>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  // const res = await getCovidContinentData()
  return {
    props: {},
  }
}

export default IndexPage
