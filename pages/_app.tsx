import type { AppProps } from 'next/app'
import {
  RecoilRoot,
  atom,
  selector,
  useRecoilState,
  useRecoilValue,
} from 'recoil'
import { ThemeProvider } from 'styled-components'
import theme from '../src/style/theme'
import GlobalStyle from '../src/style/global'
import CssBaseline from '@material-ui/core/CssBaseline'
import 'swiper/scss'
import 'swiper/scss/navigation'
import 'swiper/scss/pagination'

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <RecoilRoot>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </RecoilRoot>
    </>
  )
}
export default App
