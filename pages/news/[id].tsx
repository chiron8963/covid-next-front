import { GetServerSideProps } from 'next'
import styled from 'styled-components'
import axios from 'axios'
import { getNewsArticle } from '../../src/lib/api/news/getNewsArticle'
import { News } from '../../src/interfaces/News'
import Layout from '../../src/components/layout/Layout'
import React from 'react'
import ReactMarkdown from 'react-markdown'

const NewsDetailWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  div.header {
    display: flex;
    flex-direction: column;
    width: 768px;
  }

  .title {
    font-weight: 400;
    font-size: 3em;
    padding-top: 2em;
  }

  .createdAt {
    align-self: flex-end;
  }

  div.description {
    padding-top: 3em;
    //padding-left: 10em;
    //padding-right: 10em;
    width: 768px;

    ol {
      padding: 0;
    }
  }
`

interface NewsProps {
  news?: News
  errors?: string
}

const NewsDetail = ({ news, errors }: NewsProps) => {
  if (errors) {
    return (
      <NewsDetailWrapper>
        <p>
          <span style={{ color: 'red' }}>Error:</span> {errors}
        </p>
      </NewsDetailWrapper>
    )
  }

  return (
    <Layout>
      <NewsDetailWrapper className="global-wrapper">
        <div className="header">
          <h2 className="title">{news.title}</h2>
          <h5 className="createdAt">{news.createdAt}</h5>
        </div>
        {news.img ? <img /> : null}
        <div
          className="description"
          dangerouslySetInnerHTML={{ __html: news.content }}></div>
      </NewsDetailWrapper>
    </Layout>
  )
}

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<any> => {
  const id = params?.id

  try {
    const res = await getNewsArticle(id.toString())
    return { props: { news: res } }
  } catch (err) {
    return { props: { errors: err.message, new: null } }
  }
}

export default NewsDetail
