import Layout from '../../src/components/layout/Layout'
import React, { ReactHTML, useCallback, useEffect, useState } from 'react'
import styled from 'styled-components'
import MenuNavigation from '../../src/components/navigation/MenuNavigation'
import NewsList from '../../src/components/news/NewsList'
import { Pageing } from '../../src/interfaces/Pageing'
import { getNews } from '../../src/lib/api/news/getNews'
import { GetServerSideProps, GetStaticProps } from 'next'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useInView } from 'react-intersection-observer'
import dynamic from 'next/dynamic'
import { withRouter } from 'next/router'
import { useRecoilValue } from 'recoil'
import { searchKeywordState } from '../../src/recoil/search/searchKeywordState'

const NewsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .Target-Element {
    flex: 0;
  }
`

const NewsPageHeaderBlock = styled.div`
  h1 {
    font-size: 3.5em;
  }
`
const pageing: Pageing = { page: 0, size: 10 }

const NewsPage = ({ page_title }) => {
  const [ref, inView] = useInView()
  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [sizeChecking, setSizeChecking] = useState(false)
  const [items, setItems] = useState([])
  // const [searchKeyword, setSearchKeyword] = useState<string>(null)
  const keyword = useRecoilValue<string>(searchKeywordState)

  const getItems = useCallback(async () => {
    // await setSearchKeyword(query?.keyword)
    await setLoading(true)
    const { content, size } = await getNews(page, pageing.size, keyword)
    console.log('getItem ', keyword)
    await setItems((prevState) => [...prevState, ...content])
    size < 5 ? setSizeChecking(true) : setSizeChecking(false)
    console.log('size ', size)
    await setLoading(false)
  }, [page])

  //새로운 page 값이 있을때 call
  useEffect(() => {
    getItems()
  }, [getItems])

  //무한스크롤 하단 체크용
  useEffect(() => {
    if (inView && !loading && !sizeChecking) {
      setPage((prevState) => prevState + 1)
    }
  }, [inView, loading])

  return (
    <Layout>
      <NewsWrapper className="global-wrapper">
        <MenuNavigation />
        <NewsPageHeaderBlock>
          <h1>{page_title}</h1>
        </NewsPageHeaderBlock>
        {items && <NewsList content={items} type={'NEWS'} />}
        <div ref={ref} className="Target-Element">
          {loading && <CircularProgress />}
        </div>
      </NewsWrapper>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      page_title: '코로나 관련 소식 안내',
      data: null,
    },
  }
}

export default NewsPage
