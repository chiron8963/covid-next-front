import { GetServerSideProps, GetStaticPaths, GetStaticProps } from 'next'
import styled from 'styled-components'
import Layout from '../../src/components/layout/Layout'
import PageContentImage from '../../src/components/images/PageContentImage'
import * as React from 'react'
import { useCallback, useEffect, useState } from 'react'
import NewsLinkItem from '../../src/components/news/NewsLinkItem'
import { getCovidCountriesData } from '../../src/lib/api/country/getCovidCountriesData'
import ContentSkeleton from '../../src/components/skeleton/ContentSkeletonItem '
import { useInView } from 'react-intersection-observer'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useRouter } from 'next/router'
import CountryList from '../../src/components/countries/CountryList'
import { getContinents } from '../../src/lib/api/continent/getContinents'

const CovidCountryWrapper = styled.div`
  .Target-Element {
    width: 100%;
    height: 80px;
    display: flex;
    justify-content: center;
    text-align: center;
    align-items: center;
  }
`

const ContentsWrapper = styled.div`
  display: flex;
  height: 100%;

  .empty {
    flex: 1;
    justify-content: center;
    align-items: center;
  }
`
const CovidNewsWrapper = styled.div`
  width: 40%;
  padding: 1em 0 0 1em;
`

const CountryWrapper = styled.ul`
  padding: 4.5em 0 0;
  margin: 0;
  width: 60%;

  li {
    padding-bottom: 1em;
  }
`
export const getRouterPathName = (router) => {
  const asPathArray = router.asPath.split('/')
  const pathNameArray = router.pathname.split('/')
  return asPathArray.filter((x) => !pathNameArray.includes(x)).join('')
}

const CovidCountryDetail = ({ continentName, countries }) => {
  const pageName = '코로나 위험도 레벨 1/2 '
  const router = useRouter()
  const name = getRouterPathName(router)

  const [loading, setLoading] = useState(false)
  const [page, setPage] = useState(0)
  const [ref, inView] = useInView()
  const [sizeChecking, setSizeChecking] = useState(false)
  const [items, setItems] = useState([])

  const getItems = useCallback(async () => {
    await setLoading(true)
    const { content, size } = await getCovidCountriesData(name, page)

    await setItems((prevState) => [...prevState, ...content])
    ;(await size) < 5 ? setSizeChecking(true) : setSizeChecking(false)
    await setLoading(false)
  }, [page])

  //새로운 page 값이 있을때 call
  useEffect(() => {
    getItems()
  }, [getItems])

  //무한스크롤 하단 체크용
  useEffect(() => {
    if (inView && !loading && !sizeChecking) {
      setPage((prevState) => prevState + 1)
    }
  }, [inView, loading])

  return (
    <Layout>
      <CovidCountryWrapper className="global-wrapper">
        <PageContentImage />
        <ContentsWrapper>
          {items && <CountryList countries={items} loading={loading} />}
          <CovidNewsWrapper>
            <h2>코로나 소식</h2>
            <NewsLinkItem name={continentName} />
          </CovidNewsWrapper>
        </ContentsWrapper>
        <div ref={ref} className="Target-Element">
          {loading && <CircularProgress />}
        </div>
      </CovidCountryWrapper>
    </Layout>
  )
}

// export const getServerSideProps: GetServerSideProps = async ({params}) => {
//     const name = params?.name
//     const page = 0
//     const {content} = await getCovidCountriesData(name, page)
//     // console.log('res ', res)
//     console.log("getServerSideProps", content)
//     // const res = null
//     if (!content) {
//         return {
//             props: {
//                 continentName: name,
//                 countries: null,
//             },
//         }
//     }
//     return {
//         props: {
//             continentName: name,
//             countries: content,
//         },
//     }
// }

export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {
  return {
    paths: [], //indicates that no page needs be created at build time
    fallback: true, //indicates the type of fallback
  }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const name = params?.name
  const page = 0
  // const res = await getCovidCountriesData(name, page)

  const res = null
  if (!res) {
    return {
      props: {
        continentName: name,
        countries: null,
      },
    }
  }
  return {
    props: {
      continentName: name,
      countries: res,
    },
  }
}

export default CovidCountryDetail
