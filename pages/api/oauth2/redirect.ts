import { NextApiRequest, NextApiResponse } from 'next'
import { setResponseCookies } from '../../../src/components/utils/ResponseCookies'
import { isMember } from '../../../src/lib/api/member/isMember'
import { serialize } from 'cookie'
import { ACCESS_TOKEN, REFRESH_TOKEN } from '../../../src/constants'

const handler = async (_req: NextApiRequest, res: NextApiResponse) => {
  const token = _req.query.token
  const refreshToken = _req.query.refreshToken
  const error = _req.query.error
  const member = await isMember(token)

  if (token) {
    if (!member.socialCertification) {
      res.setHeader(
        'Set-Cookie',
        serialize(ACCESS_TOKEN, String(token), {
          path: '/',
          sameSite: 'lax',
        }),
      )

      res.redirect(`/account/outhSignup`)
    } else {
      res.setHeader(
        'Set-Cookie',
        serialize(ACCESS_TOKEN, String(token), {
          path: '/',
          sameSite: 'lax',
        }),
      )
      res.writeHead(302, { Location: '/' })
      res.end()
    }
  } else {
    return res.status(500).json({ statusCode: 500, message: error })
  }
}

export default handler
