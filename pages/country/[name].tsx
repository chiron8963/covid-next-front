import { GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import Layout from '../../src/components/layout/Layout'
import { getCities } from '../../src/lib/api/city/getCities'
import styled from 'styled-components'
import * as React from 'react'
import CityList from '../../src/components/cities/CityList'
import InformationLink from '../../src/components/navigation/InformationLink'
import SubHeader from '../../src/components/layout/SubHeader'
import Skeleton from '@material-ui/lab/Skeleton'
import usePromise from '../../src/hooks/usePromise'
import { getCovidCountryName } from '../../src/lib/api/covid/getCovidCountry'
import CovidContent from '../../src/components/contents/CovidContent'
import Paper from '@material-ui/core/Paper'
import { getCovidCountryRestriction } from '../../src/lib/api/covid/getCovidCountryRestriction'
import SkeletonCovidContent from '../../src/components/contents/SkeletonCovidContent'

const CountryWrapper = styled.div`
  flex: 1;
  width: 100%;
`

const ContentsWrapper = styled.div`
  display: flex;
`
const CovidContentBlock = styled.div`
  width: 100%;
  height: 70px;
  margin-top: 1em;
  margin-bottom: 2em;
`
const CovidEntryBlock = styled.div`
  margin-right: 1em;
`

const EntryPaper = styled(Paper)`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 100%;

  .thumbnail {
    padding: 1em;
    flex: 0.7;
    display: flex;
    justify-content: center;
    margin: auto;

    img {
      width: 25em;
      height: 13rem;
    }
  }

  .content {
    flex: 1.3;
    padding: 1em;
    margin: 1em;
  }

  .description {
    padding-bottom: 0.5rem;
    white-space: pre-wrap;
  }
`
export const LabelType = {
  NO_ENTRY: '입국 금지',
  RECOMMENDATION: '검역 강화',
  QUARANTINED: '격리 조치',
}

const restrictionType = (type) => {
  return LabelType[type]
}

// const descriptionNewLine = (description: string) => {
//     <>{description.split('\n').map((value) => {
//         return {value}
//     </br>
//     })}</>
// }
const CountryDetail = ({ cities, countryName }) => {
  const [loading, response, error] = usePromise(() => {
    return getCovidCountryName(countryName)
  }, [])

  const [covid_loading, covid_response, covid_error] = usePromise(() => {
    return getCovidCountryRestriction(response.code)
  }, [response])

  return (
    <Layout>
      <CountryWrapper className="global-wrapper">
        <div>
          {loading ? (
            <h1>
              <Skeleton variant="text" width={300} />
            </h1>
          ) : (
            response && (
              <h1>
                {response.countryName}({response.countryNameEn})
              </h1>
            )
          )}
        </div>
        <CovidContentBlock>
          {loading ? (
            <SkeletonCovidContent />
          ) : (
            response && <CovidContent item={response} />
          )}
        </CovidContentBlock>
        <CovidEntryBlock>
          <EntryPaper>
            <div className="thumbnail">
              <img
                src={'/images/illustration/alert_orange.png'}
                alt={'/images/illustration/alert_orange.png'}
              />
            </div>
            <div className="content">
              {loading ? (
                <>
                  <h3>
                    <Skeleton variant="text" width={200} />
                  </h3>
                  <h4>
                    <Skeleton variant="text" width={200} />
                  </h4>
                  <div>
                    <Skeleton variant="text" width={500} height={200} />
                  </div>
                </>
              ) : (
                <>
                  {covid_response && (
                    <h3>
                      {restrictionType(covid_response.restrictionType)} 국가
                    </h3>
                  )}
                  <h4>조치 사항</h4>
                  {covid_response ? (
                    <div className="description">
                      {covid_response.description}
                    </div>
                  ) : (
                    <>
                      <div className="description">
                        해당 국가는 확인된 정보가 없습니다.
                      </div>
                      <div className="description">
                        빠른 시일 안에 업데이트 하겠습니다.
                      </div>
                    </>
                  )}
                  {cities && (
                    <>
                      <SubHeader title={'주요 도시'} infoType={null} />
                      <CityList cities={cities} countryName={countryName} />
                    </>
                  )}
                </>
              )}
            </div>
          </EntryPaper>
        </CovidEntryBlock>
      </CountryWrapper>
    </Layout>
  )
}

export const getServerSideProps: GetServerSideProps = async ({
  params,
}): Promise<any> => {
  try {
    const name = params.name
    const res = await getCities(name)
    return { props: { cities: res, countryName: name } }
  } catch (err) {
    return { props: { errors: err.message, cities: null, countryName: null } }
  }
}

export default CountryDetail
