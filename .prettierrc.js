module.exports = {
  printWidth: 80,
  singleQuote: true,
  jsxBracketSameLine: true,
  semi: false,
  parser: "",
  endOfLine: "auto",
  tabWidth: 2,
  trailingComma: "all"
};